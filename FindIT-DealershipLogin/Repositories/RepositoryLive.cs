﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.ServiceModel;
using System.Web;
using ITP.Common.Types;
using SkyLinkWCFService.DataTypes;
using SkyLinkWCFService.DataTypes.Faults;
using SkyLinkWCFService.Services;
using VehicleManagement.Data;
using System.Configuration;
using SkyLinkWCFService.GenericGeocoder;
using System.Collections;
using Geolocation;

namespace SkyLinkWCFService.Repositories
{

    public partial class RepositoryLive : ISkyLinkService
    {
        #region Fields

        public static readonly int MAX_REPORTS = 20;

        public static readonly Guid COMPLETEDINLINE = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        public static readonly int MIN_PASSWORD_LENGTH = 6;

        private static bool isDebug = false;



        private static readonly List<State> states; // a static list of states (no state source table in database yet)

        private VehicleManagementDataContext db;
        private int? m_UserID;
        private int? m_UserAccountID;
        private VehicleManagement.Data.TimeZone userTimeZone;



        #endregion Fields

        #region Constructors

        static RepositoryLive()
        {
            states = BuildStateList();
#if DEBUG
            isDebug = true;
#endif
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="UserID"></param>

        public RepositoryLive(int UserID)
        {
            db = new VehicleManagementDataContext(isDebug);
            var user = db.UserBases.FirstOrDefault(x => x.ID == UserID);
            if (user == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.UnknownUser,
                    AdditionalInfo = "unknown user"
                }, "Unknown user.");
            }
            this.m_UserID = UserID;
            UserAccountID = user.AccountRaw.ID;
        }

        public RepositoryLive(string userName, string password)
        {
            db = new VehicleManagementDataContext(isDebug);
            db.LoginUser(userName, password, ref this.m_UserID);

            if (m_UserID == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.UnknownUser,
                    AdditionalInfo = "Login Failed"
                }, "Login failed.");
            }

            var user = db.UserBases.Single(x => x.ID == UserID);
            userTimeZone = user.TimeZone;
            UserAccountID = user.AccountRaw.ID;
            //TODO throwing an exception
            //db.InsertNow(new LoggedIn
            //{
            //  TimeStamp = DateTime.UtcNow,
            //  User = user
            //});
        }

        #endregion Constructors

        #region Properties

        public int? UserID
        {
            get
            {
                return m_UserID;
            }
            private set
            {
                m_UserID = value;
            }
        }

        public int? UserAccountID
        {
            get
            {
                return m_UserAccountID;
            }
            private set
            {
                m_UserAccountID = value;
            }
        }

        #endregion Properties

        private ShoppingCart CreateShoppingCart(Cart cart)
        {
            var a = new ShoppingCart(cart, UserAccountID);
            HttpContext.Current.Session["shoppingCart"] = a;
            return a;
        }

        private ShoppingCart GetShoppingCart()
        {
            if (HttpContext.Current.Session["shoppingCart"] == null)
                return null;

            return (ShoppingCart)HttpContext.Current.Session["shoppingCart"];
        }

        public IEnumerable<SkyLinkWCFService.DataTypes.ConsumerFaq> GetConsumerFaqs()
        {
            return db.Faqs.OfType<VehicleManagement.Data.ConsumerFaq>()
                .OrderBy(x => x.Category).ThenBy(y => y.Order)
                .Select(p => new SkyLinkWCFService.DataTypes.ConsumerFaq
                                    {
                                        Category = p.Category,
                                        Question = p.Question,
                                        Answer = p.Answer
                                    });

        }

        /// <summary>
        /// Clears all fencing violations.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>5/5/11 pp added auditing</remarks>
        ///
        public int ClearAllFencingViolations(string vin)
        {
            VehicleManagement.Data.Vehicle veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);
            var violations = veh.Locations
                            .Where(z => z.IsAcknowledged == false && (z.LocationAlertType == LocationAlertType.GeofenceEnter || z.LocationAlertType == LocationAlertType.GeofenceExit));

            foreach (Location loc in violations)
            {
                loc.IsAcknowledged = true;
            }

            db.SubmitChanges();

            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.GeofenceEnter,
                TimeStamp = DateTime.UtcNow,
                Vehicle = veh,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("All fencing violation was cleared")
            });
            return 0;

        }

        public LocationPoint ReverseGeoCode(GeoPoint geoPoint)
        {
            if (geoPoint == null || (geoPoint.Lat == 0 && geoPoint.Lon == 0))
            {
                throw C_INVALID_PARAMETER_COMBINATION;
            }
            using (var client = new GeocodingClient())
            {
                try
                {
                    var serviceReturn = client.ReverseGeocode(geoPoint.Lat, geoPoint.Lon);
                    var locationPoint = serviceReturn.ToWCFPartial();
                    locationPoint.Lat = geoPoint.Lat;
                    locationPoint.Lon = geoPoint.Lon;
                    locationPoint.TimeStamp = DateTime.UtcNow;
                    return locationPoint;
                }
                catch (FaultException<GenericGeocoder.LocationNotFound>)
                {
                    return null;
                }
            }
        }

        public LocationPoint GeoCode(string address, string city, string state, string country)
        {
            using (var client = new GeocodingClient())
            {
                try
                {
                    var a = client.ForwardGeocode(address, city, state, country);
                    var b = new LocationPoint
                    {
                        City = city,
                        State = state,
                        StreetAddress = address,
                        Country = country,
                        Lat = a.Latitude,
                        Lon = a.Longitude
                    };

                    return b;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Clears all fencing violations by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <remarks>5/5/11 pp added auditing</remarks>
        /// <remarks>10/31/2011, changed return type (void)</remarks>
        public int ClearAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            //ensure the timezone is set
            //ValidateConsumerUser();

            VehicleManagement.Data.Vehicle veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);
            var violations = veh.Locations
                            .Where(z =>
                                            z.IsAcknowledged == false &&
                                            z.TimeStamp >= userTimeZone.ToUTC(startDate) &&
                                            z.TimeStamp <= userTimeZone.ToUTC(endDate) &&
                                            (z.LocationAlertType == LocationAlertType.GeofenceEnter || z.LocationAlertType == LocationAlertType.GeofenceExit));

            foreach (Location loc in violations)
            {
                loc.IsAcknowledged = true;
            }

            db.SubmitChanges();

            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.GeofenceEnter,
                TimeStamp = DateTime.UtcNow,
                Vehicle = veh,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("User cleared fencing all fencing violations between {0} and {1}", startDate.ToString(), endDate.ToString())
            });
            return 0;

        }

        /// <summary>
        /// Acks the fencing violation.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <remarks>pp 5/5/11 added auditing </remarks>
        public void ClearFencingViolation(int id)
        {
            Location location = db.Locations.FirstOrDefault(z => (z.ID == id) && ((z.LocationAlertType == LocationAlertType.GeofenceEnter) || (z.LocationAlertType == LocationAlertType.GeofenceExit)));
            if (location == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.InternalServerError,
                    AdditionalInfo = "Fencing violation not found"
                }, "Fencing violation not found.");
            }

            var vehicle = GetConsumerVehicle(location.Vehicle.VIN, ConsumerAuthorizations.CanSetGeoFence);
            location.IsAcknowledged = true;
            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.GeofenceEnter,
                TimeStamp = DateTime.UtcNow,
                Vehicle = vehicle,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("Fencing Violation with location id:{0}, was cleared", id)
            });
        }

        /// <summary>
        /// Clears all speed alarms.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <remarks>pp 5/5/11 added auditing</remarks>
        public void ClearAllSpeedAlarms(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);
            foreach (var loc in veh.Locations.Where(z => !z.IsAcknowledged && !z.IsHidden && (z.LocationAlertType == LocationAlertType.OverSpeed)))
            {
                loc.IsAcknowledged = true;
            }
            db.SubmitChanges();
            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.Overspeed,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("All Speed Alarms were cleared for this vehicle")
            });
        }

        /// <summary>
        /// Clears all voltage alarms.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <remarks>pp 5/5/11 added auditing</remarks>
        public void ClearAllVoltageAlarms(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetBatteryAlert);
            foreach (var loc in veh.Locations.Where(z => !z.IsAcknowledged && ((z.LocationAlertType == LocationAlertType.LowBatteryLevel) || (z.LocationAlertType == LocationAlertType.PowerDisconnect))))
            {
                loc.IsAcknowledged = true;
            }
            db.SubmitChanges();

            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.LowBattery,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("All battery alarms cleared")
            });
        }

        /// <summary>
        /// Clears the speed alarm.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <remarks>pp 5/5/11 added auditing</remarks>
        public void ClearSpeedAlarm(int id)
        {
            var loc = db.Locations.FirstOrDefault(z => (z.LocationAlertType == LocationAlertType.OverSpeed) && (z.ID == id));
            if (loc == null)
            {
                throw C_LOCATION_NOT_FOUND;
            }
            else
            {
                GetConsumerVehicle(loc.Vehicle.VIN, ConsumerAuthorizations.CanSetSpeedAlert);
                loc.IsAcknowledged = true;
                db.SubmitChanges();
            }

            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.Overspeed,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = loc.Vehicle,
                OtherInfo = string.Format("Speed Alarm for locationID: {0}, cleared.", id)
            });
        }

        /// <summary>
        /// Clears the speed alarms by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <remarks>pp 5/5/2011 added auditing</remarks>
        public void ClearSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);
            foreach (var loc in veh.Locations.Where(z => !z.IsAcknowledged && !z.IsHidden && (z.LocationAlertType == LocationAlertType.OverSpeed) && (z.TimeStamp >= userTimeZone.ToUTC(startDate)) && (z.TimeStamp <= userTimeZone.ToUTC(endDate))))
            {
                loc.IsAcknowledged = true;
            }
            db.SubmitChanges();

            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.Overspeed,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("Speed Alarms between the dates(UTC): {0} and {1} where cleared", startDate.ToString(), endDate.ToString())
            });
        }

        /// <summary>
        /// Clears the voltage alarm.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <remarks>pp 5/5/2011 added auditing</remarks>
        public void ClearVoltageAlarm(int id)
        {
            var loc = db.Locations.FirstOrDefault(z => !z.IsAcknowledged && (z.ID == id) && ((z.LocationAlertType == LocationAlertType.PowerDisconnect) || (z.LocationAlertType == LocationAlertType.LowBatteryLevel)));
            if (loc == null)
            {
                throw C_LOCATION_NOT_FOUND;
            }
            GetConsumerVehicle(loc.Vehicle.VIN, ConsumerAuthorizations.CanSetBatteryAlert);
            loc.IsAcknowledged = true;
            db.SubmitChanges();

            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.LowBattery,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = loc.Vehicle,
                OtherInfo = string.Format("The Battery Alarm with location id: {0} was cleared.", id)
            });
        }

        /// <summary>
        /// Clears the voltage alarms by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <remarks>pp 5/5/2011 added auditing</remarks>
        public void ClearVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetBatteryAlert);
            foreach (var loc in veh.Locations.Where(z => !z.IsAcknowledged && ((z.LocationAlertType == LocationAlertType.LowBatteryLevel) || (z.LocationAlertType == LocationAlertType.PowerDisconnect)) && (z.TimeStamp >= userTimeZone.ToUTC(startDate)) && (z.TimeStamp <= userTimeZone.ToUTC(endDate))))
            {
                loc.IsAcknowledged = true;
            }
            db.SubmitChanges();
            db.InsertNow(new AlertAcknowledged
            {
                AlertType = AlertType.LowBattery,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("Battery Alarms between the dates(UTC): {0} and {1} where cleared", startDate.ToString(), endDate.ToString())
            });
        }

        //TODO flesh out this method
        public int CreateSkyLinkUser(DataTypes.SkyLinkUserInformation skyLinkUserInformation, string newPassword)
        {
            return -1;
            //var user = db.ConsumerUsers.FirstOrDefault(z => z.ID == UserID);
            //if (!user.Authorization.HasFlag(ConsumerAuthorizations.CanManageSubUsers))
            //{
            //  throw C_INSUFFICIENT_PERMISSIONS;
            //}

            ////TODO: validation???
            //var user = db.ConsumerUsers.FirstOrDefault(z => z.ID == UserID);
            //if (!user.Authorization.HasFlag(ConsumerAuthorizations.CanManageSubUsers))
            //{
            //    throw C_INSUFFICIENT_PERMISSIONS;
            //}

            ////create authorizations
            ////TODO: add other auth flags
            //ConsumerAuthorizations auth = ConsumerAuthorizations.NONE;
            //if (skyLinkUserInformation.CanAddVehicles)
            //{
            //  auth |= ConsumerAuthorizations.CanAddVehicles;
            //}
            //if (skyLinkUserInformation.CanManageUsers)
            //{
            //  auth |= ConsumerAuthorizations.CanManageSubUsers;
            //}
            //if (skyLinkUserInformation.CanRenewSubscription)
            //{
            //  auth |= ConsumerAuthorizations.CanManageSubscriptions;
            //}

            ////actually add it
            //int? newUserID = -1;
            //int retval = db.CreateUser(
            //    skyLinkUserInformation.FirstName,
            //    skyLinkUserInformation.LastName,
            //    skyLinkUserInformation.EmailAddress,
            //    skyLinkUserInformation.AlternateEmail,
            //    skyLinkUserInformation.CellPhone,
            //    skyLinkUserInformation.CellProvider,
            //    skyLinkUserInformation.PrimaryPhone,
            //    newPassword,
            //    (int)UserType.ConsumerUser,
            //    null,
            //    string.Empty,
            //    (int)userTimeZone,
            //    user.Account.ID,
            //    (int)auth, 0, ref newUserID);

            ////check for error
            //if (retval != 0)
            //{
            //  throw new FaultException<GeneralFault>(new GeneralFault
            //  {
            //    FaultType = GeneralFaultType.InternalServerError,
            //    AdditionalInfo = "Error creating new user"
            //  });
            //}
            //        skyLinkUserInformation.PrimaryPhone,
            //        newPassword,
            //        (int)UserType.ConsumerUser,
            //        null,
            //        string.Empty,
            //        (int)userTimeZone,
            //        user.Account.ID,
            //        (int)auth, 0, ref newUserID);

            ////set audit log
            //db.InsertNow(new UserCreated
            //{
            //  FirstUser = db.ConsumerUsers.First(z => z.ID == UserID),
            //  SecondUser = db.ConsumerUsers.First(y => y.ID == UserID),
            //  TimeStamp = DateTime.UtcNow
            //});
            //    });
            //}

            ////return user id
            //return newUserID.Value;
        }

        public Guid EnableSpeedAlert(string vin, bool enable)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);
            veh.CheckForDevice();

            //check to see if it is already in the state requested
            if (veh.SpeedAlertEnabled == enable)
            {
                return COMPLETEDINLINE;
            }

            //create the device command
            DeviceCommand cmd;
            if (enable)
            {
                if (!veh.SpeedAlertKPH.HasValue)
                {
                    //throw new FaultException<GeneralFault>(new GeneralFault
                    //{
                    //FaultType = GeneralFaultType.InvalidParameterCombination,
                    //AdditionalInfo = "Cannot enable a speed alert which has never been set"
                    //});
                    veh.SpeedAlertKPH = 24;
                }
                cmd = new SetOverspeed
                {
                    CommandStatus = CommandStatus.InProgress,
                    Device = veh.Device,
                    ReceivedTime = null,
                    SentTime = DateTime.UtcNow,
                    Speed = veh.SpeedAlertKPH.Value
                };
            }
            else
            {
                cmd = new DisableOverspeed
                {
                    CommandStatus = CommandStatus.InProgress,
                    Device = veh.Device,
                    ReceivedTime = null,
                    SentTime = DateTime.UtcNow
                };
            }

            //save the command
            /* Not sure about this block... Don't want to log twice! (See a few lines below.)
            db.InsertOnSubmit(new OverspeedAdjustmentRequested
            {
                TimeStamp = DateTime.UtcNow,
                User = db.ConsumerUsers.First(z => z.ID == UserID),
                Vehicle = veh
            });*/
            db.InsertNow(cmd);

            //send the command
            cmd.SendToDTS();

            db.InsertNow(new OverspeedAdjustmentRequested
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("Overspeed state change requested. New Value: {0}", enable)
            });

            //return command id
            return cmd.CommandID;
        }

        public void EndTrackingSession(string vin)
        {
            throw new FaultException<GeneralFault>(new GeneralFault
            {
                FaultType = GeneralFaultType.NotImplemented,
                AdditionalInfo = "This feature has not been implemented"
            }, "Tracking feature has not been implemented.");
        }

        /// <summary>
        /// Gets all fencing violations.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>pp Added auditing and limited reports to MAX_REPORTS</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetAllFencingViolations(string vin)
        {
            var vehicle = GetConsumerVehicle(vin);

            //audit it
            db.InsertNow(new AlertsRequested
            {
                AlertType = AlertType.GeofenceEnter,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = vehicle,
                OtherInfo = string.Format("User requested ALL fencing violations. NOTE there is no type for all so entry type will be set to GeofenceEnter")
            });

            return vehicle.Locations.OrderByDescending(desc => desc.TimeStamp).Where(
                                            z => (z.LocationAlertType == LocationAlertType.GeofenceEnter ||
                                                            z.LocationAlertType == LocationAlertType.GeofenceExit) &&
                                                            !z.IsHidden).Take(MAX_REPORTS).ToWCF();
        }

        /// <summary>
        /// Gets all fencing violations by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        /// <remarks>PP Added auditing and limited returns to MAX_REPORTS</remarks>
        ///
        public IEnumerable<DataTypes.LocationPoint> GetAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            var vehicle = GetConsumerVehicle(vin);

            //audit it
            db.InsertNow(new AlertsRequested
            {
                AlertType = AlertType.GeofenceEnter,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = vehicle,
                OtherInfo = string.Format("User requested fencing violations between the dates(UTC): {0} and {1}", startDate.ToString(), endDate.ToString())
            });

            return vehicle.Locations.OrderByDescending(desc => desc.TimeStamp).Where(
                                            z => (z.LocationAlertType == LocationAlertType.GeofenceEnter || z.LocationAlertType == LocationAlertType.GeofenceExit) &&
                                                            (z.TimeStamp >= userTimeZone.ToUTC(startDate) && z.TimeStamp <= userTimeZone.ToUTC(endDate)) &&
                                                            !z.IsHidden
                                                            ).Take(MAX_REPORTS).ToWCF();
        }


        /// <summary>
        /// Gets events, optionally filtered by time and date.
        /// </summary>
        /// <param name="vFilter">Vehicle filters to apply to the set of vehicles to be returned.</param>
        /// <param name="eFilter">Event filter to apply to the set of vehicles to be returned.</param>
        /// <param name="lastLocation">Flag indicating whether or not to include last location in returned events.</param>
        /// <param name="userType">User type flag (HACK).</param>
        /// <returns></returns>
        /// <remarks>GO 3/20/12 added</remarks>
        public VehicleEventsResponse GetEvents(VehicleFilter vFilter, EventFilter eFilter, Boolean lastLocation, int userType = 0)
        {
            # region Gary's code.
            //            // Set defaults where invalid
            //            if (vFilter == null) { vFilter = new VehicleFilter(); }
            //            if (eFilter == null) { eFilter = new EventFilter(); }

            //            if (vFilter.maxCount < 1)
            //            {
            //                vFilter.maxCount = 20;
            //            }

            //            if (vFilter.maxCount > 30)
            //            {
            //                vFilter.maxCount = 30;
            //            }

            //            if (eFilter.maxCount < 1)
            //            {
            //                eFilter.maxCount = 1;
            //            }

            //            if (eFilter.maxCount > 20)
            //            {
            //                eFilter.maxCount = 20;
            //            }

            //            if (eFilter.maxCount * vFilter.maxCount > 60)
            //            {
            //                vFilter.maxCount = 20;
            //                eFilter.maxCount = 3;
            //            }

            //            if (lastLocation == null)
            //            {
            //                lastLocation = false;
            //            }


            //            // Determine date range and set to min/max if not provided
            //            if ((eFilter.startDate == null) || (eFilter.startDate == DateTime.MinValue))
            //            {
            //                eFilter.startDate = DateTime.MinValue;
            //            }

            //            if ((eFilter.endDate == null) || (eFilter.endDate == DateTime.MinValue))
            //            {
            //                eFilter.endDate = DateTime.MaxValue;
            //            }

            //            // Add log entry
            //            db.InsertNow(new AlertsRequested
            //            {
            //                TimeStamp = DateTime.UtcNow,
            //                User = db.UserBases.Where(x => x.ID == UserID).Single(),
            //                AlertType = AlertType.GeofenceEnter,
            //                OtherInfo = string.Format("User requested events between {0} and {1}", eFilter.startDate.ToString(), eFilter.endDate.ToString())
            //            });

            //            // HACK: As an interim solution for DriveTime, we will only return vehicles where plans match the "userType" parameter.
            //            //       Ordinarily, we would return all consumer vehicles on the account.
            //            //
            //            int planRestriction = 0;
            //            switch (userType)
            //            {
            //                case 1: // Repo: the vehicle must have a "DriveTime Reactivated" plan (ID=9) attached
            //                    planRestriction = 9;
            //                    break;
            //                case 2: // Dealer: the vehicle must have a "DealerLink Fleet" plan (ID=3) attached
            //                    planRestriction = 3;
            //                    break;
            //            }

            //            // If an eFilter is specified, convert it to an array of LocationAlertTypes
            //            LocationAlertType[] eFilterArray = null;

            //            if ((eFilter.locationAlertTypes != null) && (eFilter.locationAlertTypes.Length > 0))
            //            {
            //                Dictionary<string, LocationAlertType> eventTypes = new Dictionary<string, LocationAlertType>();
            //                eventTypes.Add("Location", LocationAlertType.None);
            //                eventTypes.Add("IgnOn", LocationAlertType.IgnitionOn);
            //                eventTypes.Add("IgnOff", LocationAlertType.IgnitionOff);
            //                eventTypes.Add("EnterGeo", LocationAlertType.GeofenceExit);// is this a bug?
            //                eventTypes.Add("ExitGeo", LocationAlertType.GeofenceEnter);// or this
            //                eventTypes.Add("Speed", LocationAlertType.OverSpeed);
            //                eventTypes.Add("Battery", LocationAlertType.LowBatteryLevel);
            //                eventTypes.Add("QuickFence", LocationAlertType.EarlyTheftDetection);
            //                eventTypes.Add("Power", LocationAlertType.PowerDisconnect);

            //                List<LocationAlertType> eFilterList = new List<LocationAlertType>();
            //                foreach (string eventType in eFilter.locationAlertTypes)
            //                {
            //                    eFilterList.Add(eventTypes[eventType.Trim()]);
            //                }
            //                eFilterArray = eFilterList.ToArray();
            //            }

            //            string query = string.Format(@"
            //declare @vMax int; set @vMax = {0};
            //declare @eMax int; set @eMax = {1};
            //declare @recsMax int; set @recsMax = {0} * {1};
            //set ROWCOUNT @recsMax;
            //select rank, PK_Location,TimeStamp, ReceivedTimeStamp, Longitude, Latitude, StreetAddress, City, State, PostalCode, County, Country, Speed, Heading, IgnitionOn, LocationAlertType, AlertIndex, TripDuration, TripOdometer, TripMaxSpeed, IsAcknowledged, IsHidden, FK_VehicleID, VIN from (
            //  select row_number() over (partition by fk_vehicleid order by receivedtimestamp desc) as rank, *
            //  from location left join vehicle on fk_vehicleid=pk_vehicleid where fk_vehicleid in ( ", eFilter.maxCount, vFilter.maxCount);


            //            // Determine the set of vehicles we are working with
            //            IQueryable<VehicleManagement.Data.Vehicle> vehicleList;

            //            if (planRestriction > 0)
            //            {      // If there is a plan restriction, apply it ensuring that the plan is not expired
            //                query = query + string.Format("select pk_vehicleid from vehicle join device on fk_device=pk_deviceid where fk_dealerid={0} and (isSold='False' or loantype=1) and isorphaned='false' ", UserAccountID);

            //            }
            //            else
            //            {                    // Otherwise get all matching vehicles on the account
            //                vehicleList = GetConsumerUser().Vehicles;
            //                query = query + string.Format("select pk_vehicleid from vehicle where fk_ownerid = {0} ", UserAccountID);
            //            }

            //            // Filter by VIN
            //            if ((vFilter.vins != null) && (vFilter.vins.Length > 0))
            //            {       // If VIN filter was specified, match only those VINs
            //                query = query + string.Format(" and vin in ('{0}')", string.Join("','", vFilter.vins.Select(p => p.ToString()).ToArray()));
            //            }

            //            // Filter by area
            //            if (vFilter.area != null)
            //            {
            //                float radius = (float)vFilter.area.Radius / 1110;
            //                query = query + string.Format(" and (latitude between {0} and {1}) and (longitude between {2} and {3})",
            //                    vFilter.area.Lat - radius,
            //                    vFilter.area.Lat + radius,
            //                    vFilter.area.Lon - radius,
            //                    vFilter.area.Lon + radius);
            //            }

            //            query = query + ")";

            //            // Filter by event type
            //            if (eFilterArray != null)
            //            {
            //                query = query + string.Format(" and locationalerttype in ({0})", string.Join(",", eFilterArray.Select(p => p.GetHashCode().ToString()).ToArray()));
            //            }

            //            // Filter by start date
            //            if (eFilter.startDate > DateTime.MinValue)
            //            {
            //                query = query + string.Format(" and timestamp > '{0}'", eFilter.startDate.ToString());
            //            }

            //            // Filter by end date
            //            if (eFilter.endDate < DateTime.MaxValue)
            //            {
            //                   query = query + string.Format(" and timestamp < '{0}'", eFilter.endDate.ToString());
            //            }

            //            // Filter by area
            //            if (eFilter.area != null)
            //            {
            //                float radius = (float)eFilter.area.Radius / 1110;
            //                query = query + string.Format(" and (latitude between {0} and {1}) and (longitude between {2} and {3})",
            //                    eFilter.area.Lat - radius,
            //                    eFilter.area.Lat + radius,
            //                    eFilter.area.Lon - radius,
            //                    eFilter.area.Lon + radius);
            //            }

            //            // Filter by acknowledgement type
            //            if (eFilter.ackType != null)
            //            {
            //                switch (eFilter.ackType.ToLower())
            //                {
            //                    case "ack":
            //                        query = query + " and isAcknowledged='true'";
            //                        break;
            //                    case "nack":
            //                        query = query + " and isAcknowledged='false'";
            //                        break;
            //                    default:
            //                        // All other values are treated as "all"
            //                        break;
            //                }
            //            }

            //            query = query + string.Format(") events where events.rank <= {0} order by events.vin, events.rank; set rowcount 0;", eFilter.maxCount);

            //            var results = db.ExecuteQuery<Location>(query);



            #endregion

            DateTime minDate = DateTime.UtcNow.AddDays(-1).Date;
            DateTime maxDate = DateTime.UtcNow.Date.AddDays(1);
           

            VehicleEventsResponse response = new VehicleEventsResponse();

            //var vehicles = from r in results
            //               group r by r.Vehicle into v
            //               select new { veh = v.Key, events = v };

            CoordinateBoundaries coordBound = null;
            try
            {
                coordBound = new CoordinateBoundaries(vFilter.area.Lat, vFilter.area.Lon, vFilter.area.Radius < 6 ? 10 : vFilter.area.Radius);
            }catch(Exception e) //TODO log it
            {
                return null;
            }


            var vehicles = db.DealerUsers.First(du => du.ID == this.UserID)
            .AccountRaw.Vehicles
            .SelectMany(x => x.vMostRecentAlerts)
            .Where(xx =>
                xx.LocationAlertType == LocationAlertType.LowBatteryLevel &&
                xx.TimeStamp >= minDate &&
                xx.TimeStamp <= maxDate &&
                xx.Latitude > coordBound.MinLatitude &&
                xx.Latitude < coordBound.MaxLatitude &&
                xx.Longitude > coordBound.MinLongitude &&
                xx.Longitude < coordBound.MaxLongitude);
            

            foreach (var v in vehicles)
            {
                VehicleEvents vResult = new VehicleEvents(v.Vehicle.VIN);
                List<Location> vEvents = new List<Location>();

                //if (lastLocation)                                           // If we requested the most recent location to be included,
                //{
                //    vEvents.Add(v.Vehicle.MostRecentLocation);                  // Then always include most current vehicle location
                //    if (eFilterArray.Length == 1)
                //    {
                //        vEvents.First().LocationAlertType = eFilterArray[0];    // and, if we filtered by a single event type, then make it look like that.
                //    }
                //}
                vEvents.Add(v.Location); //customized for lowbattery, we only want the one location
                response.Add(v.Vehicle.ToWCF(), new VehicleEvents(v.Vehicle.VIN, vEvents.ToWCF().ToArray()));
            }
            return response;
        }



        /// <summary>
        /// Gets all speed alarms.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>PP 5/5/11 added auditing and limited returns to MAX_REPORTS</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetAllSpeedAlarms(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);

            db.InsertNow(new AlertsRequested
            {
                AlertType = AlertType.Overspeed,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("User requested ALL OverSpeed alarms")
            });

            return veh.Locations.OrderByDescending(dt => dt.TimeStamp).Where(z => !z.IsAcknowledged && !z.IsHidden && (z.LocationAlertType == LocationAlertType.OverSpeed))
                    .Take(MAX_REPORTS).ToWCF();
        }

        /// <summary>
        /// Gets all speed alarms by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        /// <remarks>pp 5/5/11 added auditing, limited returns to MAX_REPORTS</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetAllSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);

            db.InsertNow(new AlertsRequested
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                AlertType = AlertType.GeofenceEnter,
                OtherInfo = string.Format("User requested overspeed alarms between {0} and {1}", startDate.ToString(), endDate.ToString())
            });

            return veh.Locations.OrderByDescending(desc => desc.TimeStamp).Where(z => !z.IsAcknowledged && !z.IsHidden && (z.LocationAlertType == LocationAlertType.OverSpeed) && (z.TimeStamp >= userTimeZone.ToUTC(startDate)) && (z.TimeStamp <= userTimeZone.ToUTC(endDate))).Take(MAX_REPORTS).ToWCF();
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        /// <remarks>pp 5/5/11 Added auditing</remarks>
        public IDictionary<int, DataTypes.SkyLinkUserInformation> GetAllUsers()
        {
            var user = db.ConsumerUsers.FirstOrDefault(z => z.ID == UserID);
            if (!user.Authorization.HasFlag(ConsumerAuthorizations.CanManageSubUsers))
            {
                throw C_INSUFFICIENT_PERMISSIONS;
            }

            db.InsertNow(new UserInfoRequest
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                TimeStamp = DateTime.UtcNow,
                OtherInfo = string.Format("GetAllUsers() executed")
            });
            return user.Account.Users.ToWCF();
        }

        /// <summary>
        /// Gets all voltage alarms.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>5/5/11 pp added auditing, limited returns to MAX_REPORTS</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetAllVoltageAlarms(string vin)
        {
            var veh = GetConsumerVehicle(vin);

            db.InsertNow(new AlertsRequested
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                TimeStamp = DateTime.UtcNow,
                AlertType = AlertType.GeofenceEnter,
                OtherInfo = string.Format("GetAllVoltageAlarms() called")
            });

            return veh.Locations.OrderByDescending(desc => desc.TimeStamp).Where(z => !z.IsAcknowledged && !z.IsHidden && ((z.LocationAlertType == LocationAlertType.LowBatteryLevel) || (z.LocationAlertType == LocationAlertType.PowerDisconnect))).Take(MAX_REPORTS).ToWCF();
        }

        /// <summary>
        /// Gets all voltage alarms by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing, limited returns to MAX_REPORTS</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetAllVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            var veh = GetConsumerVehicle(vin);
            db.InsertNow(new AlertsRequested
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                TimeStamp = DateTime.UtcNow,
                AlertType = AlertType.GeofenceEnter,
                OtherInfo = string.Format("GetAllVoltageAlarmsByDate({0},{1},{2}) called", vin, startDate.ToString(), endDate.ToString())
            });

            return veh.Locations.OrderByDescending(desc => desc.TimeStamp).Where(z => !z.IsAcknowledged && !z.IsHidden && ((z.LocationAlertType == LocationAlertType.LowBatteryLevel) || (z.LocationAlertType == LocationAlertType.PowerDisconnect)) && (z.TimeStamp >= userTimeZone.ToUTC(startDate)) && (z.TimeStamp <= userTimeZone.ToUTC(endDate))).Take(MAX_REPORTS).ToWCF();
        }

        /// <summary>
        /// Gets the battery voltage alarm level.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public float? GetBatteryVoltageAlarmLevel(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetBatteryAlert);

            db.InsertNow(new VehiclePropertyRequest
            {
                Vehicle = veh,
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("GetBatteryVoltageAlarmLevel({0}) called, {1} was returned", vin, veh.BatteryVoltageAlarmLevel)
            });

            return veh.BatteryVoltageAlarmLevel;
        }

        /// <summary>
        /// Gets the cell phone providers.
        /// </summary>
        /// <returns></returns>
        /// 5.5.11 pp don't think we need to audit this one
        public IEnumerable<string> GetCellPhoneProviders()
        {
            return db.CellPhoneProviders.Where(y => y.IsActive).OrderBy(z => z.Name).Select(x => x.Name);
        }

        /// <summary>
        /// Gets the command status.
        /// </summary>
        /// <param name="commandId">The command id.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public DataTypes.ECommandStatus GetCommandStatus(Guid commandId)
        {
            if (commandId == COMPLETEDINLINE)
            {
                return DataTypes.ECommandStatus.Success;
            }

            var cmd = db.DeviceCommands.FirstOrDefault(z => z.CommandID == commandId);
            if (cmd == null)
            {
                throw C_COMMAND_NOT_FOUND;
            }
            GetConsumerVehicle(cmd.Device.Vehicle.VIN);

            ECommandStatus status = cmd.CommandStatus.ToWCF();

            db.InsertNow(new DeviceCommandStatusRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("CommandStatus({0} requested, returned: {1}", commandId.ToString(), status.ToString())
            });

            return status;
        }

        /// <summary>
        /// Gets the customer info.
        /// </summary>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public SkyLinkUserInformation GetCustomerInfo()
        {
            SkyLinkUserInformation slu = new SkyLinkUserInformation();

            var user = db.ConsumerUsers.Where(x => x.ID == UserID).FirstOrDefault();

            db.InsertNow(new UserInfoRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("GetCustomerInfo() called, returned info for user:{0}, with NetSuiteID of: {1}", user.ID.ToString(), user.NetSuiteID)
            });

            return user.ToWCF();
        }

        /// <summary>
        /// Gets the dashboard.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public DataTypes.DashBoardInfo GetDashboard(string vin)
        {
            return null;
            #region Not used in find it.
            //var veh = GetConsumerVehicle(vin);

            //bool isBatteryAlarmed = false;
            //bool isGeoFenceViolated = false;
            //bool isSpeedAlarmed = false;
            //bool isQuickFenceViolated = false;

            ////get the latest locations of type
            //var latBatloc = veh.Locations.OrderBy(z => z.TimeStamp)
            //    .Where(x => x.LocationAlertType == LocationAlertType.LowBatteryLevel).FirstOrDefault();

            //var geoFenceVio = veh.Locations.OrderBy(z => z.TimeStamp).Where(x =>
            //            x.LocationAlertType == LocationAlertType.GeofenceEnter || x.LocationAlertType == LocationAlertType.GeofenceExit).FirstOrDefault();

            //var speedAlarmVio = veh.Locations.OrderBy(z => z.TimeStamp)
            //                                                .Where(x => x.LocationAlertType == LocationAlertType.OverSpeed).FirstOrDefault();

            //var quickFenceVio = veh.Locations.OrderBy(z => z.TimeStamp)
            //                                                .Where(x => x.LocationAlertType == LocationAlertType.EarlyTheftDetection).FirstOrDefault();

            ////set out local values
            //if (latBatloc != null)
            //    isBatteryAlarmed = !latBatloc.IsAcknowledged;
            //if (geoFenceVio != null)
            //    isGeoFenceViolated = !geoFenceVio.IsAcknowledged;
            //if (speedAlarmVio != null)
            //    isSpeedAlarmed = !speedAlarmVio.IsAcknowledged;
            //if (quickFenceVio != null)
            //    isQuickFenceViolated = !quickFenceVio.IsAcknowledged;

            //var retVal = new DashBoardInfo(vin)
            //{
            //    BatteryLevelSetting = GetBatteryVoltageAlarmLevel(vin),
            //    CurrentUserInfomation = GetCustomerInfo(),
            //    GeoFences = GetGeofences(vin),
            //    InsuranceCard = new InsuranceCard
            //    {
            //        AgentName = veh.Agent,
            //        AgentPhoneNumber = veh.AgentPhone,
            //        Dealership = veh.Device.Dealer.ToWCF(),
            //        InsuranceCarrier = veh.InsuranceCarrier,
            //        OwnerInformation = GetConsumerUser().ToWCF(),
            //        PolicyNumber = veh.PolicyNumber,
            //        Vehicle = veh.ToWCF()
            //    },
            //    BatteryAlertEnabled = veh.BatteryAlertEnabled,
            //    SpeedAlertEnabled = veh.SpeedAlertEnabled,
            //    IsBatteryAlarmed = isBatteryAlarmed,
            //    IsQuickFenceSet = veh.IsQuickFenceSet,
            //    IsQuickFenceViolated = isQuickFenceViolated,
            //    IsSpeedAlarmed = isSpeedAlarmed,
            //    LastLocation = veh.Locations.OrderByDescending(z => z.TimeStamp).FirstOrDefault().ToWCFType(),
            //    LatestTripsSummary = GetMostRecientNTripSummarys(vin, 5),
            //    NumberOfGeofencesSupported = veh.Device.DeviceType.NumberGeoFences,
            //    SpeedAlertValue = veh.SpeedAlertKPH,
            //    SupportedFenceTypes = veh.Device.DeviceType.SupportedFenceTypes.ToWCFCollection()
            //};

            //db.InsertNow(new UserInfoRequest
            //{
            //    TimeStamp = DateTime.UtcNow,
            //    User = db.UserBases.Where(x => x.ID == UserID).Single(),
            //    OtherInfo = string.Format("GetDashboard({0}) called", vin)
            //});
            //			return retVal;
            #endregion
        }

        /// <summary>
        /// Gets the geofences.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public IEnumerable<DataTypes.Geofence> GetGeofences(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);

            List<DataTypes.Geofence> fenceList = new List<DataTypes.Geofence>();

            foreach (VehicleManagement.Data.GeoFence fence in veh.GeoFences.Where(z => z.Account.GetType() == typeof(ConsumerAccount)).OrderByDescending(ob => ob.ID))
            {
                var newFence = fence.ToWCFType();
                fenceList.Add(newFence);
            }

            db.InsertNow(new VehicleGeofenceRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetGeofences({0} called. {1} fences returned", vin, fenceList.Count)
            });

            return fenceList;
        }

        /// <summary>
        /// Gets the maintenance interval definitions for an account.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT", true)]
        public IEnumerable<DataTypes.MaintenanceInterval> GetMaintenanceIntervals()
        {
			throw new NotImplementedException();
			//db.InsertNow(new UserPropertyRequest
			//{
			//	TimeStamp = DateTime.UtcNow,
			//	User = db.UserBases.Where(x => x.ID == UserID).Single(),
			//	OtherInfo = string.Format("GetMaintenanceIntervals() called")
			//});

			//foreach (var interval in GetConsumerUser().Account.MaintenanceIntervals)
			//{
			//	yield return interval.ToWCFType();
			//}
        }

        /// <summary>
        /// Deletes maintenance interval definition(s) from an account.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool DeleteMaintenanceIntervals(int[] intervalIDs)
        {
            db.InsertNow(new UserPropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("DeleteMaintenanceIntervals() called for ID " + intervalIDs.Select(i => i.ToString()))
            });

            foreach (int i in intervalIDs)
            {
                VehicleManagement.Data.MaintenanceInterval interval = GetDealerUser().Account.MaintenanceIntervals.Where(x => x.ID == i).Single();
                db.MaintenanceIntervals.DeleteOnSubmit(interval);
            }
            db.SubmitChanges();

            return true;
        }

        /// <summary>
        /// Updates an array of maintenance interval definition(s) on an account.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT", true)]
		public bool UpdateMaintenanceIntervals(SkyLinkWCFService.DataTypes.MaintenanceInterval[] intervals)
        {
			throw new NotImplementedException();

			//db.InsertNow(new UserPropertyRequest
			//{
			//	TimeStamp = DateTime.UtcNow,
			//	User = db.UserBases.Where(x => x.ID == UserID).Single(),
			//	OtherInfo = string.Format("DeleteMaintenanceIntervals() called for ID " + intervals.Select(i => i.ToString()))
			//});

			//foreach (SkyLinkWCFService.DataTypes.MaintenanceInterval i in intervals)
			//{
			//	VehicleManagement.Data.MaintenanceInterval interval = GetConsumerUser().Account.MaintenanceIntervals.Where(x => x.ID == i.ID).Single();
			//	i.ToDBType(interval);
			//	interval.AccountID = GetConsumerUser().Account.ID;

			//	// TBD: Technically, I guess we should maintain a list of modified interval objects and just do one submit.
			//	// However, the UI is currently defined to only allow one update at a time and, to be honest, I'm not entirely
			//	// sure that there will be much difference between me initiating multiple submits and LINQ (I assume it would need
			//	// to write each record individually, although likely within a single transaction).  I'll look more closely at that
			//	// when I'm not under the gun to get this finished...
			//	db.SubmitChanges();
			//}

			//return true;
        }

        /// <summary>
        /// Adds an array of maintenance interval definitions to an account.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT", true)]
		public IEnumerable<DataTypes.MaintenanceInterval> AddMaintenanceIntervals(SkyLinkWCFService.DataTypes.MaintenanceInterval[] intervals)
        {
			throw new NotImplementedException();
			//db.InsertNow(new UserPropertyRequest
			//{
			//	TimeStamp = DateTime.UtcNow,
			//	User = db.UserBases.Where(x => x.ID == UserID).Single(),
			//	OtherInfo = string.Format("DeleteMaintenanceIntervals() called for ID " + intervals.Select(i => i.ToString()))
			//});

			//foreach (SkyLinkWCFService.DataTypes.MaintenanceInterval i in intervals)
			//{
			//	VehicleManagement.Data.MaintenanceInterval interval = new VehicleManagement.Data.MaintenanceInterval();
			//	i.ToDBType(interval);
			//	GetConsumerUser().Account.MaintenanceIntervals.Add(interval);
			//}
			//db.SubmitChanges();

			//return GetMaintenanceIntervals();
        }

        /// <summary>
        /// Gets the maintenance intervals for a vehicle.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public IEnumerable<DataTypes.VehicleMaintenanceInterval> GetVehicleMaintenanceIntervals(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);

            List<DataTypes.VehicleMaintenanceInterval> intervalList = new List<DataTypes.VehicleMaintenanceInterval>();

            foreach (VehicleManagement.Data.VehicleMaintenanceInterval interval in veh.VehicleMaintenanceIntervals.OrderBy(ob => ob.IntervalIndex))
            {
                var newInterval = interval.ToWCFType(GetDealerUser().ID);
                intervalList.Add(newInterval);
            }

            //TBD: Fix this with a correct audit log entry type.
            db.InsertNow(new VehicleGeofenceRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetVehicleMaintenanceIntervals({0} called. {1} interrvals returned", vin, intervalList.Count)
            });

            return intervalList;
        }

        /// <summary>
        /// Updatesthe maintenance intervals for a vehicle.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="maintenanceIntervalID">Array of IDs to associate with this vehicle.</param>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT",true)]
        public IEnumerable<DataTypes.VehicleMaintenanceInterval> UpdateVehicleMaintenanceIntervals(string vin, int?[] maintenanceIntervalID)
        {
			throw new NotImplementedException();
			//var vehicle = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetMaintenanceIntervals);

			//// Must count backwards since we may be removing intervals from our vehicle.
			//for (int i = maintenanceIntervalID.Length - 1; i >= 0; i--)
			//{
			//	// If the intervalID is null, then we should delete it...
			//	if (maintenanceIntervalID[i] == null)
			//	{
			//		// ... but only if it exists.
			//		if (i < vehicle.VehicleMaintenanceIntervals.Count)
			//		{
			//			// Remove the entry from the database
			//			db.VehicleMaintenanceIntervals.DeleteAllOnSubmit(db.VehicleMaintenanceIntervals.Where(m => (m.VehicleID == vehicle.VehicleID && m.IntervalIndex == i)));

			//			// And remove it from out entity set
			//			vehicle.VehicleMaintenanceIntervals.RemoveAt(i);

			//			// Finally, decrement the interval index of subsequent items in the entityset
			//			// to account for the loss of this one.
			//			for (var j = i; j < vehicle.VehicleMaintenanceIntervals.Count; j++)
			//			{
			//				vehicle.VehicleMaintenanceIntervals[j].IntervalIndex--;
			//			}
			//		}
			//	}
			//	else // Otherwise (not null) we need to update or add it
			//	{
			//		// If this interval previously existed,then just update it...
			//		if (i < vehicle.VehicleMaintenanceIntervals.Count)
			//		{
			//			// ...but only if it changed.
			//			if (vehicle.VehicleMaintenanceIntervals[i].MaintenanceIntervalID != maintenanceIntervalID[i])
			//			{
			//				vehicle.VehicleMaintenanceIntervals[i].MaintenanceIntervalID = (int)maintenanceIntervalID[i];
			//				vehicle.VehicleMaintenanceIntervals[i].StartByUser = GetConsumerUser().ID;
			//				vehicle.VehicleMaintenanceIntervals[i].StartDate = DateTime.UtcNow;

			//				vehicle.VehicleMaintenanceIntervals[i].RecordedDate = vehicle.MetricsTimestamp ?? DateTime.UtcNow;
			//				vehicle.VehicleMaintenanceIntervals[i].RecordedOdometer = vehicle.MetricsOdometer ?? 0;
			//				vehicle.VehicleMaintenanceIntervals[i].RecordedUtilization = vehicle.MetricsUtilization ?? 0;
			//				vehicle.VehicleMaintenanceIntervals[i].CalculateDue();
			//			}
			//		}
			//		else
			//		{
			//			// ...otherwise we need to create a new one and add it.
			//			SkyLinkWCFService.DataTypes.VehicleMaintenanceInterval interval = new SkyLinkWCFService.DataTypes.VehicleMaintenanceInterval();
			//			interval.MaintenanceIntervalID = (int)maintenanceIntervalID[i];
			//			interval.Index = i;

			//			VehicleManagement.Data.VehicleMaintenanceInterval dbInterval = interval.ToDBType();
			//			dbInterval.Vehicle = vehicle;
			//			dbInterval.VehicleID = vehicle.VehicleID;
			//			dbInterval.StartByUser = GetConsumerUser().ID;
			//			dbInterval.StartDate = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Unspecified);         // Important to specify "unspecified" kind to match the results we get from SQL Server
			//			dbInterval.RecordedDate = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Unspecified);      // Important to specify "unspecified" kind to match the results we get from SQL Server
			//			dbInterval.MaintenanceInterval = db.MaintenanceIntervals.Where(m => (m.ID == dbInterval.MaintenanceIntervalID)).FirstOrDefault();
			//			dbInterval.CalculateDue();

			//			vehicle.VehicleMaintenanceIntervals.Add(dbInterval);
			//		}
			//	}
			//}

			//db.SubmitChanges();

			//db.InsertNow(new GeofenceSetRequested
			//{
			//	User = db.ConsumerUsers.FirstOrDefault(z => z.ID == UserID),
			//	TimeStamp = DateTime.UtcNow,
			//	Vehicle = vehicle,
			//	OtherInfo = string.Format("UpdateVehicleMaintenanceIntervals({0},{1})", vin, maintenanceIntervalID.ToString())
			//});

			//return GetVehicleMaintenanceIntervals(vin);
        }

        /// <summary>
        /// Moves the detail of a maintenance intervals to the log and resets the maintenance intervals.
        /// </summary>
        /// <param name="vin">VIN of the vehicle being modified.</param>
        /// <param name="intervalIndexes">Array of interval indexes.</param>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT", true)]
        public bool LogVehicleMaintenanceIntervals(string vin, int[] intervalIndexes)
        {
			throw new NotImplementedException();
			//var vehicle = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetMaintenanceIntervals);

			//for (int i = 0; i < intervalIndexes.Length; i++)
			//{
			//	int index = intervalIndexes[i];

			//	if (index < vehicle.VehicleMaintenanceIntervals.Count)
			//	{
			//		LogVehicleMaintenanceInterval(vehicle.VehicleMaintenanceIntervals[index]);
			//		ResetVehicleMaintenanceInterval(vehicle.VehicleMaintenanceIntervals[index]);
			//	}
			//}

			//db.SubmitChanges();

			//db.InsertNow(new GeofenceSetRequested
			//{
			//	User = db.ConsumerUsers.FirstOrDefault(z => z.ID == UserID),
			//	TimeStamp = DateTime.UtcNow,
			//	Vehicle = vehicle,
			//	OtherInfo = string.Format("LoggedVehicleMaintenanceIntervals({0},{1})", vin, intervalIndexes.ToString())
			//});

			//return true;
        }


        /// <summary>
        /// Performs the moving of maintenance intervals to the log.
        /// </summary>
        /// <param name="maintenanceInterval">maintenanceInterval to add to the log.</param>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT", true)]
        private bool LogVehicleMaintenanceInterval(VehicleManagement.Data.VehicleMaintenanceInterval maintenanceInterval)
        {
			throw new NotImplementedException();
			//VehicleManagement.Data.VehicleLog log = new VehicleManagement.Data.VehicleLog();
			//VehicleManagement.Data.Vehicle vehicle = maintenanceInterval.Vehicle;
			//log.VehicleID = vehicle.VehicleID;

			//log.Name = maintenanceInterval.MaintenanceInterval.Name;
			//log.Description = maintenanceInterval.MaintenanceInterval.Description;

			//log.StartDate = maintenanceInterval.StartDate;
			//log.StartOdometer = maintenanceInterval.StartOdometer;
			//log.StartUtilization = maintenanceInterval.StartUtilization;
			//log.StartUserID = maintenanceInterval.StartByUser;

			//log.DueDate = maintenanceInterval.DueDate;
			//log.DueOdometer = maintenanceInterval.DueOdometer;
			//log.DueUtilization = maintenanceInterval.DueUtilization;

			//log.EndDate = DateTime.UtcNow;
			//log.EndOdometer = maintenanceInterval.RecordedOdometer;
			//log.EndUtilization = maintenanceInterval.RecordedUtilization;
			//log.EndUserID = GetConsumerUser().ID;

			//vehicle.VehicleLogs.Add(log);

			//return true;
        }

        /// <summary>
        /// Resets a vehicle's maintenance interval.
        /// </summary>
        /// <param name="maintenanceInterval">maintenanceInterval to add to the log.</param>
        /// <returns></returns>
        /// <remarks></remarks>
		[Obsolete("not used in FindIT",true)]
        private bool ResetVehicleMaintenanceInterval(VehicleManagement.Data.VehicleMaintenanceInterval maintenanceInterval)
        {
			throw new NotImplementedException();
            // Specifically, this function takes the most current vehicle metrics and writes those as the Start and Recorded 
            // values in the VehicleMaintenance, which is the log of maintenance intervals that are current.
			//VehicleManagement.Data.Vehicle vehicle = maintenanceInterval.Vehicle;

			//maintenanceInterval.StartByUser = GetConsumerUser().ID;

			//maintenanceInterval.StartDate = DateTime.UtcNow;
			//maintenanceInterval.StartOdometer = vehicle.MetricsOdometer ?? 0;
			//maintenanceInterval.StartUtilization = vehicle.MetricsUtilization ?? 0;

			//maintenanceInterval.RecordedDate = maintenanceInterval.StartDate;
			//maintenanceInterval.RecordedOdometer = maintenanceInterval.StartOdometer;
			//maintenanceInterval.RecordedUtilization = maintenanceInterval.StartUtilization;

			//maintenanceInterval.CalculateDue();

			//return true;
        }

        /// <summary>
        /// Gets the last location.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public DataTypes.LocationPoint GetLastLocation(string vin)
        {
            var veh = GetConsumerVehicle(vin);

            var topLoc = veh.MostRecentLocation;

            if (topLoc == null)
                return null;

            return topLoc.ToWCFType();//nifty
        }

        /// <summary>
        /// Gets the last N locations. limited to MAX_LOCATIONS
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="numberOfLocations">The number of locations.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing, limited number of locations returned</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetLastNLocations(string vin, int numberOfLocations)
        {
            int numberToReturn = numberOfLocations > MAX_REPORTS ? MAX_REPORTS : numberOfLocations;

            var veh = GetConsumerVehicle(vin);
            var retVal = veh.Locations.OrderByDescending(z => z.TimeStamp).Take(numberToReturn).ToWCF();

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetLastNLocations({0},{1}) called, {2} locations returned, {3} requested",
                vin, numberToReturn, retVal.Count(), numberOfLocations)
            });

            return retVal;
        }

        /// <summary>
        /// Gets the locations after id. Limit to MAX_LOCATIONS
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="locationId">The location id.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetLocationsAfterId(string vin, int locationId)
        {
            var veh = GetConsumerVehicle(vin);

            veh.CheckForDevice();

            Location previous = veh.Locations.FirstOrDefault(z => z.ID == locationId);

            if (previous == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.InvalidParameterCombination,
                    AdditionalInfo = "Location not found for vehicle"
                }, "Location not found for vehicle.");
            }
            //return veh.Locations.Where(loc => loc.TimeStamp >= previous.TimeStamp).ToWCF();
            var retVal = veh.Locations.OrderByDescending(desc => desc.TimeStamp).Where(loc => loc.ID > locationId);

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetLocationsAfterId({0},{1}) called, locations in set {2}, set truncated:{3}",
                        vin, locationId, retVal.Count(), retVal.Count() > 20)
            });

            return retVal.Take(20).ToWCF();
        }

        /// <summary>
        /// Gets the locations by date.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetLocationsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            if (startDate >= endDate)
                throw C_INVALID_PARAMETER_COMBINATION;

            var veh = GetConsumerVehicle(vin);

            veh.CheckForDevice();

            var retVal = veh.Locations.Where(loc =>
                            loc.TimeStamp >= userTimeZone.ToUTC(startDate) && loc.TimeStamp <= userTimeZone.ToUTC(endDate)
                                            );

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetLocationsByDate({0},{1},{2}) called, locations in set {3}, set truncated:{4}",
                vin, startDate, endDate, retVal.Count(), retVal.Count() > MAX_REPORTS)
            });

            return retVal.Take(2500).ToWCF(); // .Take(MAX_REPORTS was removed because we need all of 'em
        }

        /// <summary>
        /// Gets the locations since.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 pp added auditing</remarks>
        public IEnumerable<DataTypes.LocationPoint> GetLocationsSince(string vin, DateTime startDate)
        {
            var veh = GetConsumerVehicle(vin);
            veh.CheckForDevice();

            var retVal = veh.Locations.OrderByDescending(desc => desc.TimeStamp).Where(loc => loc.TimeStamp > userTimeZone.ToUTC(startDate));

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetLocationsSince({0},{1}) called, locations in set {2}, set truncated:{3}",
                vin, startDate, retVal.Count(), retVal.Count() > MAX_REPORTS)
            });

            return retVal.Take(MAX_REPORTS).ToWCF();
        }

        /// <summary>
        /// Gets the most recent trip.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>added auditing pp 5.5.11</remarks>
        public DataTypes.Trip GetMostRecentTrip(string vin)
        {
            var veh = GetConsumerVehicle(vin);

            //get last ignition off
            var tripEnd = veh.Locations.OrderByDescending(z => z.TimeStamp).FirstOrDefault(z => z.LocationAlertType == LocationAlertType.IgnitionOff);
            if (tripEnd == null)
            {
                db.InsertNow(new VehicleLocationRequest
                {
                    TimeStamp = DateTime.UtcNow,
                    User = db.UserBases.Where(x => x.ID == UserID).Single(),
                    Vehicle = veh,
                    OtherInfo = string.Format("GetMostRecentTrip({0}) null trip returned", vin)
                });
                return null;
            }

            //get previous ignition on
            var tripStart = veh.Locations.Where(z => z.TimeStamp < tripEnd.TimeStamp).OrderByDescending(y => y.TimeStamp).FirstOrDefault(x => x.LocationAlertType == LocationAlertType.IgnitionOn);
            if (tripEnd == null)
            {
                db.InsertNow(new VehicleLocationRequest
                {
                    TimeStamp = DateTime.UtcNow,
                    User = db.UserBases.Where(x => x.ID == UserID).Single(),
                    Vehicle = veh,
                    OtherInfo = string.Format("GetMostRecentTrip({0}) null trip returned", vin)
                });
                return null;
            }

            //fill and return trip
            Trip trip;
            if (tripEnd.TripOdometer.HasValue)
            {
                trip = new Trip(tripStart.ToWCFType(), tripEnd.ToWCFType(), tripEnd.TripOdometer.Value);
            }
            else
            {
                trip = new Trip(tripStart.ToWCFType(), tripEnd.ToWCFType());
            }

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetMostRecentTrip({0}) trip returned start:{0} end:{1}", trip.TripStart.ToString(), tripEnd.ToString())
            });
            return trip;
        }

        /// <summary>
        /// Gets the most recient N trip summarys.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="numberOfTrips">The number of trips.</param>
        /// <returns></returns>
        /// <remarks>5.5.11 added auditing pp</remarks>
        public IEnumerable<DataTypes.TripSummary> GetMostRecientNTripSummarys(string vin, int numberOfTrips)
        {
            return null;

            var veh = GetConsumerVehicle(vin);

            //var blaa = new List<DataTypes.TripSummary>();

            //var bleet = new DataTypes.TripSummary();

            Stack<DataTypes.TripSummary> tripStack = new Stack<TripSummary>();

            var offOns = veh.Locations.OrderByDescending(x => x.TimeStamp)
.Where(x => x.LocationAlertType == LocationAlertType.IgnitionOff && x.TimeStamp > DateTime.Parse("09/02/2011"));

            Location loc = null;

            foreach (Location l in offOns)
            {
                if (tripStack.Count == numberOfTrips)
                    break;

                if (l.TripDuration == null)
                    continue;

                if (loc == null)
                {
                    loc = l;
                    continue;
                }

                var ts = new TripSummary
                {
                    TripEnd = loc.ToWCFType(),
                    MaxSpeed = loc.TripMaxSpeed,
                    Distance = loc.TripOdometer,
                    Duration = loc.TripDuration.Value,
                    TripStart = l.ToWCFType()
                };

                ts.TripStart.TimeStamp = ts.TripEnd.TimeStamp - ts.Duration;

                tripStack.Push(ts);

                loc = l;


            }


            //foreach (Location loc in veh.Locations
            //                .Where(p => p.LocationAlertType == LocationAlertType.IgnitionOn || p.LocationAlertType == LocationAlertType.IgnitionOff)
            //                .OrderByDescending(x => x.TimeStamp))
            //{
            //    if (loc.LocationAlertType == LocationAlertType.IgnitionOff)
            //    {
            //        //if we get another ign off before finishing trip, ignore it
            //        if (tripStack.Count != 0 && tripStack.Peek().TripStart == null)
            //            continue;

            //        tripStack.Push(new TripSummary
            //        {
            //            TripEnd = loc.ToWCFType(),
            //            MaxSpeed = loc.TripMaxSpeed,
            //            Distance = loc.TripOdometer,
            //            Duration = loc.TripDuration == null ? new TimeSpan() : (TimeSpan)loc.TripDuration
            //        });

            //        continue;
            //    }

            //    //found an off, if we've a trip without an off, give it an off
            //    if (
            //                    loc.LocationAlertType == LocationAlertType.IgnitionOn &&
            //                    tripStack.Count() > 0 &&
            //                    tripStack.Peek().TripStart == null)
            //    {
            //        tripStack.Peek().TripStart = loc.ToWCFType();
            //    }

            //    if (tripStack.Count == numberOfTrips && tripStack.Peek().TripStart != null)
            //    {
            //        //we're done
            //        break;
            //    }
            //}

            //drop any dangling trip
            //if (tripStack.Count != 0 && tripStack.Peek().TripStart == null)
            //{
            //    tripStack.Pop();
            //}

            //sigh if there isn't any trips send em a null;
            if (tripStack.Count == 0)
                return null;

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetMostRecientNTripSummarys({0},{1}) called, # trips returned:", vin, numberOfTrips, tripStack.Count())
            });

            return tripStack.ToList();
        }

        /// <summary>
        /// Gets the number of geofences supported.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public int GetNumberOfGeofencesSupported(string vin)
        {
            var device = GetConsumerVehicle(vin).Device;

            if (device == null)
            {
                throw C_DEVICE_NOT_INSTALLED;
            }

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = device.Vehicle,
                OtherInfo = string.Format("GetNumberOfGeofencesSupported({0}) called, returned:{1}:", vin, device.DeviceType.NumberGeoFences.ToString())
            });

            return device.DeviceType.NumberGeoFences;
        }

        /// <summary>
        /// Gets the speed alert setting.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public int? GetSpeedAlertSetting(string vin)
        {
            var veh = GetConsumerVehicle(vin);
            veh.CheckForDevice();

            db.InsertNow(new VehiclePropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GeSpeedAlertSetting({0} called, returned:{1}", vin, veh.SpeedAlertKPH)
            });

            return veh.SpeedAlertKPH;
        }

        public IEnumerable<State> GetStates()
        {
            return states;
        }

        /// <summary>
        /// Gets the supported geofence types.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public IEnumerable<DataTypes.EFenceGeometery> GetSupportedGeofenceTypes(string vin)
        {
            var device = GetConsumerVehicle(vin).Device;
            if (device == null)
            {
                throw C_DEVICE_NOT_INSTALLED;
            }

            db.InsertNow(new VehiclePropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = device.Vehicle,
                OtherInfo = string.Format("GetSupportedGeofenceTypes({0} called", vin)
            });

            return device.DeviceType.ToWCFType();
        }

        /// <summary>
        /// Gets the trip.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public DataTypes.TripInfo GetTrip(string vin, DateTime startDate)
        {
            //get vehicle
            var veh = GetConsumerVehicle(vin);

            //get trip start
            var tripStart = veh.Locations.FirstOrDefault(z => (z.LocationAlertType == LocationAlertType.IgnitionOn) && (z.TimeStamp == userTimeZone.ToUTC(startDate)));
            if (tripStart == null)
            {
                db.InsertNow(new VehicleLocationRequest
                {
                    TimeStamp = DateTime.UtcNow,
                    User = db.UserBases.Where(x => x.ID == UserID).Single(),
                    Vehicle = veh,
                    OtherInfo = string.Format("GetTrip({0},{1}) null trip returned", vin, startDate.ToString())
                });
                return null;
            }

            //get trip end
            var tripEnd = veh.Locations.Where(z => z.TimeStamp > tripStart.TimeStamp).OrderBy(y => y.TimeStamp).FirstOrDefault(x => x.LocationAlertType == LocationAlertType.IgnitionOff);
            if (tripEnd == null)
            {
                db.InsertNow(new VehicleLocationRequest
                {
                    TimeStamp = DateTime.UtcNow,
                    User = db.UserBases.Where(x => x.ID == UserID).Single(),
                    Vehicle = veh,
                    OtherInfo = string.Format("GetTrip({0},{1}) null trip returned", vin, startDate.ToString())
                });
                return null;
            }

            //get inner locations
            var tripLocations = veh.Locations.Where(z => (z.TimeStamp > tripStart.TimeStamp) && (z.TimeStamp < tripEnd.TimeStamp));

            //fill trip info
            var retVal = new TripInfo
            {
                Distance = tripEnd.TripOdometer,
                Duration = tripEnd.TimeStamp - tripStart.TimeStamp,
                MaxSpeed = tripEnd.TripMaxSpeed,
                TripEnd = tripEnd.ToWCFType(),
                TripLocations = tripLocations.ToWCF(),
                TripStart = tripStart.ToWCFType()
            };

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetTrip({0},{1}) trip returned Distance: Duration: MaxSpeed: TripStart: TripEnd: #Locations: ",
                vin, startDate.ToString(), retVal.Distance, retVal.Duration.ToString(), retVal.MaxSpeed, retVal.TripStart, retVal.TripEnd, retVal.TripLocations.Count())
            });

            return retVal;
        }

        /// <summary>
        /// Gets the trips summaries.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="maxReports">The max reports.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        /// <remarks>changed how trips are calculated</remarks>
        public IEnumerable<DataTypes.TripSummary> GetTripsSummaries(string vin, DateTime startDate, DateTime endDate, int maxReports)
        {
            if (startDate >= endDate)
                throw C_INVALID_PARAMETER_COMBINATION;

            if (maxReports > MAX_REPORTS)
                maxReports = MAX_REPORTS;

            var veh = GetConsumerVehicle(vin);

            //get potential trip ends
            //var potentialTrips = veh.Locations
            //                .Where(z => (z.TimeStamp >= userTimeZone.ToUTC(startDate))
            //                                                                && (z.TimeStamp <= userTimeZone.ToUTC(endDate))
            //                                                                && (z.LocationAlertType == LocationAlertType.IgnitionOff))
            //                .Select(y => new
            //                {
            //                    End = y,
            //                    Start = veh.Locations
            //                                    .Where(x => x.TimeStamp < y.TimeStamp)
            //                                    .OrderByDescending(w => w.TimeStamp)
            //                                    .FirstOrDefault(v => v.LocationAlertType == LocationAlertType.IgnitionOn)
            //                })
            //                .Where(u => u.Start != null);

            var locations = veh.Locations.Where(ts => ts.TimeStamp >= userTimeZone.ToUTC(startDate)
                    && ts.TimeStamp <= userTimeZone.ToUTC(endDate)
                    && (ts.LocationAlertType == VehicleManagement.Data.LocationAlertType.IgnitionOff || ts.LocationAlertType == VehicleManagement.Data.LocationAlertType.IgnitionOn)).OrderByDescending(l => l.TimeStamp);

            var realTrips = new List<TripSummary>();
            TripSummary tmpTripSummary = null;
            foreach (var loc in locations)
            {
                if (loc.LocationAlertType == VehicleManagement.Data.LocationAlertType.IgnitionOff && (tmpTripSummary == null || tmpTripSummary.TripEnd != null))
                {
                    tmpTripSummary = new TripSummary();
                    tmpTripSummary.TripEnd = loc.ToWCFType();
                    tmpTripSummary.Distance = loc.TripOdometer;
                    tmpTripSummary.MaxSpeed = loc.TripMaxSpeed;
                }
                else if (tmpTripSummary != null && loc.LocationAlertType == VehicleManagement.Data.LocationAlertType.IgnitionOn)
                {
                    tmpTripSummary.TripStart = loc.ToWCFType();
                    tmpTripSummary.Duration = tmpTripSummary.TripEnd.TimeStamp - loc.TimeStamp;
                    realTrips.Add(tmpTripSummary);
                    tmpTripSummary = null;
                }
                //if (realTrips.Count == maxReports)
                //{
                //	break;
                //}
            }

            //ensure that trips don't overlap
            DateTime previousStartTime = DateTime.MinValue;

            //foreach (var potential in potentialTrips.OrderBy(z => z.Start.TimeStamp))
            //{
            //    if (potential.Start.TimeStamp != previousStartTime)
            //    {
            //        realTrips.Add(new TripSummary
            //        {
            //            TripStart = potential.Start.ToWCFType(),
            //            TripEnd = potential.End.ToWCFType(),
            //            Distance = potential.End.TripOdometer,
            //            Duration = potential.End.TimeStamp - potential.Start.TimeStamp,
            //            MaxSpeed = potential.End.TripMaxSpeed
            //        });
            //        if (realTrips.Count == maxReports)
            //        {
            //            break;
            //        }
            //        previousStartTime = potential.Start.TimeStamp;
            //    }
            //}

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("GetTripsSummaries({0},{1},{2},{3}) called, #returned: {4}",
                vin, startDate.ToString(), endDate.ToString(), maxReports, realTrips.Count())
            });

            //return list
            return realTrips;
        }

        /// <summary>
        /// Gets the vehicles.
        /// </summary>
        /// <returns></returns>
        /// <remarks>added auditing, should probable improve audit info, or create a new type</remarks>
        public IEnumerable<DataTypes.Vehicle> GetVehicles()
        {
            db.InsertNow(new UserPropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("GetVehicles() called")
            });
            foreach (var veh in GetDealerUser().Vehicles)
            {
                yield return veh.ToWCF();
            }
        }

        /// <summary>
        /// Gets the vehicles.
        /// </summary>
        /// <returns></returns>
        /// <remarks>added auditing, should probable improve audit info, or create a new type</remarks>
        public IEnumerable<DataTypes.Vehicle> GetVehicleByVIN(string vin)
        {
            db.InsertNow(new UserPropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("GetVehiclesByVIN() called")
            });

            var vehicle = GetConsumerVehicle(vin);
            yield return vehicle.ToWCF();
        }

        public Dictionary<string, GeoPoint> GetAccessableDealerships()
        {


            var retVal = new Dictionary<string, GeoPoint>();

            var acnt = db.DealerUsers.FirstOrDefault(x => x.ID == this.UserID);

            if (acnt == null) return null;

            //check to see if there's security group membership

            if (acnt.GroupAccount == null)
            {
                // "Single Account".Dump();
                //var cAccount = acnt as DealershipAccount;
                //if it's single and not a DealershipAccount, return null.
                if (acnt == null || !acnt.Account.HomeLatitude.HasValue || !acnt.Account.HomeLongitude.HasValue) return null;



                //if account doesn't have location return null
                retVal.Add(acnt.Account.CompanyName, new GeoPoint(acnt.Account.HomeLatitude.Value, acnt.Account.HomeLongitude.Value));

                return retVal;
            }

            //"Multi Account".Dump();
            var entries = acnt.AccountRaw.DealershipSecurityGroupMembersRaw
                    .Where(x => x.Dealership.HomeLatitude.HasValue && x.Dealership.HomeLongitude.HasValue)
                    .OrderBy(x => x.Dealership.CompanyName)
                    .Select(x => new DictionaryEntry(x.Dealership.CompanyName, new GeoPoint(x.Dealership.HomeLatitude.Value, x.Dealership.HomeLongitude.Value)));

            //entries.Dump("dumped value");

            foreach (var a in entries)
            {
                retVal.Add((string)a.Key, (GeoPoint)a.Value);
            }

            return retVal;
        }

        /// <summary>
        /// Gets the vehicles.
        /// </summary>
        /// <returns></returns>
        /// <remarks>added auditing, should probable improve audit info, or create a new type</remarks>
        public IEnumerable<DataTypes.Vehicle> GetVehiclesByVINSuffix(string vinSuffix, int userType)
        {
            db.InsertNow(new UserPropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                OtherInfo = string.Format("GetVehiclesByVINSuffix() called")
            });

            // HACK: As an interim solution for DriveTime, we will only return vehicles where:
            //         1) The VIN ends with the specified suffix
            //         2) The vehicle has a "DriveTime Reactivated" plan (ID=9) attached
            //         3) The plan is not expired
            //
            //       Ordinarily, we would return all consumer vehicles with the specified suffix,
            //       in which case, uncomment the following line and remove the definition of vehicleList and subsequent for statement.
            //
            switch (userType)
            {
                case 0: // Consumer: return all vehicles on the account where the VIN ends with the specified suffix
                    foreach (var veh in GetDealerUser().Vehicles.Where(z => (z.VIN.EndsWith(vinSuffix))))
                    {
                        yield return veh.ToWCF();
                    }
                    break;
                case 1: // Repo: return vehicles on the account where:
                    //         1) The VIN ends with the specified suffix
                    //         2) The vehicle has a "DriveTime Reactivated" plan (ID=9) attached
                    //         3) The plan is not expired
                    var vehicleList1 = from veh in GetDealerUser().Vehicles.Where(z => (z.VIN.EndsWith(vinSuffix)))
                                       join plan in db.LinkSubscriptionMaps.Where(z => (z.LinkSubscriptionPlan.ID == 9) && (z.PlanExpiration > DateTime.UtcNow)) on veh.VIN equals plan.Vehicle.VIN
                                       select new { ve = veh };

                    foreach (var veh in vehicleList1)
                    {
                        yield return veh.ve.ToWCF();
                    }
                    break;
                case 2: // Dealer: return vehicles on the account where:
                    //         1) The VIN ends with the specified suffix
                    //         2) The vehicle has a "DealerLink Fleet" plan (ID=3) attached
                    //         3) The plan is not expired
                    var vehicleList2 = from veh in GetDealerUser().Vehicles.Where(z => (z.VIN.EndsWith(vinSuffix)))
                                       join plan in db.LinkSubscriptionMaps.Where(z => (z.LinkSubscriptionPlan.ID == 3) && (z.PlanExpiration > DateTime.UtcNow)) on veh.VIN equals plan.Vehicle.VIN
                                       select new { ve = veh };

                    foreach (var veh in vehicleList2)
                    {
                        yield return veh.ve.ToWCF();
                    }
                    break;
            }
        }

        /// <summary>
        /// Returns vehicles where either: 1) the last 5 (or more) of the VIN contains the search term, or 2) the Stock Number contains the search term.
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public IEnumerable<DataTypes.Vehicle> GetFindITVehicles(string searchTerm)
        {
            foreach (var veh in GetDealerUser().Vehicles.Where(z => (z.VIN.Contains(searchTerm) || z.StockNumber.Contains(searchTerm))))
            {
                yield return veh.ToWCF();
            }
        }

        /// <summary>
        /// Sets the state of the battery alert.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <returns></returns>
        public Guid SetBatteryAlertState(string vin, bool enable)
        {
            var vehicle = GetConsumerVehicle(vin);
            //send the command to set voltage to 1
            //if the command was a success, update the db to BatteryAlertEnabled = state
            // else return command failed or some shit
            // retun the command id

            if (vehicle.BatteryVoltageAlarmLevel == null)
                vehicle.BatteryVoltageAlarmLevel = 11;//throw C_VALIDATION_FAILURE;

            DeviceCommand cmd = null;

            if (enable)
            {
                cmd = new EnableBatteryAlarm();
            }
            else
            {
                cmd = new DisableBatteryAlarm();
            }

            cmd.CommandStatus = CommandStatus.InProgress;
            cmd.Device = vehicle.Device;

            db.InsertNow(cmd);
            cmd.SendToDTS();

            db.InsertNow(new BatteryAlertEnableDisableRequest
            {
                User = GetDealerUser(),
                Vehicle = vehicle,
                OtherInfo = string.Format("SetSpeedAlertState(vehicle={0} enable={1}", vehicle.VIN, enable)
            });

            return cmd.CommandID;
        }

        /// <summary>
        /// Determines whether [is battery alert enabled] [the specified vin].
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns>
        /// 	<c>true</c> if [is battery alert enabled] [the specified vin]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsBatteryAlertEnabled(string vin)
        {
            var vehicle = GetConsumerVehicle(vin);
            return vehicle.BatteryAlertEnabled;
        }

        public LocationPoint GetMostRecentLocationByType(string vin, DataTypes.LocationAlarmType locationType)
        {
            var vehicle = GetConsumerVehicle(vin);

            var result = db.Locations.OrderByDescending(loc => loc.TimeStamp)
                    .Where(t => t.LocationAlertType == locationType.ToDBType()).FirstOrDefault();

            return result.ToWCFType();
        }

        /// <summary>
        /// Determines whether [is quick fence set] [the specified vin].
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns>
        /// 	<c>true</c> if [is quick fence set] [the specified vin]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.VehicleFault">VehicleFault</exception>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.GeneralFault">GeneralFault</exception>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        ///
        public bool IsQuickFenceSet(string vin)
        {
            //ValidateConsumerUser();

            //var vehicle = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);
			var vehicle = GetDealershipVehicle(vin, DealerAuthorizations.ManageVehicle);

            vehicle.CheckForDevice();

            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = vehicle,
                OtherInfo = string.Format("IsQuickFenceSet({0} called, result:{1}", vin, vehicle.IsQuickFenceSet)
            });

            return vehicle.IsQuickFenceSet;
        }

        /// <summary>
        /// Determines whether [is speed alert enabled] [the specified vin].
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns>
        /// 	<c>true</c> if [is speed alert enabled] [the specified vin]; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>added auditing pp 5.5.11</remarks>
        public bool IsSpeedAlertEnabled(string vin)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);
            db.InsertNow(new VehicleLocationRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                OtherInfo = string.Format("IsSpeedAlertEnabled({0}) called, result:{1}", vin, veh.SpeedAlertEnabled)
            });
            return veh.SpeedAlertEnabled;
        }

        /// <summary>
        /// Removes the geofence.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="fenceId">The fence id.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public Guid RemoveGeofence(string vin, int fenceId)
        {
            var vehicle = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);

            vehicle.CheckForDevice();

            //ensure fence exists
            var fence = vehicle.GeoFences.FirstOrDefault(z => z.FenceIndex == fenceId);
            if (fence == null)
            {
                //nothing to do
                db.InsertNow(new VehiclePropertyRequest
                {
                    TimeStamp = DateTime.UtcNow,
                    User = db.UserBases.Where(x => x.ID == UserID).Single(),
                    Vehicle = vehicle,
                    OtherInfo = string.Format("RemoveGeofence({0},{1} called, result: nop, no fence at the index", vin, fenceId)
                });
                return COMPLETEDINLINE;
            }

            //check for disabled fence
            if (!fence.Enabled)
            {
                db.DeleteNow(fence);
                db.InsertNow(new VehiclePropertyRequest
                {
                    TimeStamp = DateTime.UtcNow,
                    User = db.UserBases.Where(x => x.ID == UserID).Single(),
                    Vehicle = vehicle,
                    OtherInfo = string.Format("RemoveGeofence({0},{1} called, result: fence deleted, no command needed, existed in db only", vin, fenceId)
                });
                return COMPLETEDINLINE;
            }

            //create db command
            DeviceCommand cmd = null;
            switch (fence.FenceType)
            {
                case GeoFenceType.Circular:
                    cmd = new DisableCircularFence
                    {
                        CommandStatus = CommandStatus.InProgress,
                        Device = vehicle.Device,
                        FenceIndex = fence.FenceIndex,
                        ReceivedTime = null,
                        SentTime = DateTime.UtcNow
                    };
                    break;
                case GeoFenceType.Rectangular:
                    cmd = new DisableRectangularFence
                    {
                        CommandStatus = CommandStatus.InProgress,
                        Device = vehicle.Device,
                        FenceIndex = fence.FenceIndex,
                        ReceivedTime = null,
                        SentTime = DateTime.UtcNow
                    };
                    break;
                case GeoFenceType.Poly:
                    cmd = new DisablePolyFence
                    {
                        CommandStatus = CommandStatus.InProgress,
                        Device = vehicle.Device,
                        FenceIndex = fence.FenceIndex,
                        ReceivedTime = null,
                        SentTime = DateTime.UtcNow
                    };
                    break;
                default:
                    throw new FaultException<DeviceFault>(new DeviceFault
                    {
                        FaultType = DeviceFaultType.UnsupportedFenceType,
                        AdditionalInfo = "Fence Type Unknown"
                    }, "Fence type unknown.");
            }
            db.InsertOnSubmit(new GeofenceSetRequested
            {
                Vehicle = vehicle,
                TimeStamp = DateTime.UtcNow,
                User = db.ConsumerUsers.First(z => z.ID == UserID)
            });
            db.InsertNow(cmd);

            //send to device
            cmd.SendToDTS();

            db.InsertNow(new VehiclePropertyRequest
            {
                TimeStamp = DateTime.UtcNow,
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = vehicle,
                OtherInfo = string.Format("RemoveGeofence({0},{1} called, result: command sent to device: {2}", vin, fenceId, cmd.CommandID.ToString())
            });

            //return command ID
            return cmd.CommandID;
        }

        /// <summary>
        /// Requests the location update.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        /// HACKED FOR FIND IT
        public Guid RequestLocationUpdate(string vin)
        {
            var veh = GetDealershipVehicle(vin, DealerAuthorizations.ManageVehicle);
            veh.CheckForDevice();

            var command = new RequestLocationCommand();
            command.Device = veh.Device;
            command.CommandStatus = CommandStatus.InProgress;
            command.SentTime = DateTime.UtcNow;
            db.InsertNow(command);

            command.SendToDTS();

            db.InsertNow(new LocationUpdateRequest
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                TimeStamp = DateTime.UtcNow,
                OtherInfo = string.Format("RequestLocationUpdate({0}) called, result:{1}", vin, command.CommandID.ToString())
            });

            return command.CommandID;
        }

        /// <summary>
        /// Sets the battery voltage alarm level.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="level">The level.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public Guid SetBatteryVoltageAlarmLevel(string vin, float level)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetBatteryAlert);
            veh.CheckForDevice();

            if (!veh.BatteryAlertEnabled)
            {
                veh.BatteryVoltageAlarmLevel = level;
                return COMPLETEDINLINE;
            }

            //create command
            var cmd = new SetBatteryAlarmLevel
            {
                CommandStatus = CommandStatus.InProgress,
                Device = veh.Device,
                ReceivedTime = null,
                SentTime = DateTime.UtcNow,
                Threshold = level
            };

            //store it
            db.InsertNow(cmd);

            //send it
            cmd.SendToDTS();

            db.InsertNow(new LowBatteryLevelSetRequest
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                TimeStamp = DateTime.UtcNow,
                OtherInfo = string.Format("SetBatteryVoltageAlarmLevel({0},{1}) called, result:{2}", vin, level.ToString(), cmd.CommandID.ToString())
            });

            //return command ID
            return cmd.CommandID;
        }

        /// <summary>
        /// Sets the geofence.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="index">The index.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <returns></returns>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.VehicleFault">VehicleFault</exception>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.GeneralFault">GeneralFault</exception>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public Guid SetGeofence(string vin, int index, bool enable)
        {
            //ValidateConsumerUser();

            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);

            veh.CheckForDevice();

            var fence = veh.GeoFences.Where(v => v.FenceIndex == index).FirstOrDefault();

            if (fence == null)
            {
                throw new FaultException<DeviceFault>(new DeviceFault
                {
                    FaultType = DeviceFaultType.GeofenceDoesNotExist
                }, "Geofence does not exist.");
            }

            ITP.Common.Types.GeoFence iFence = null;

            DeviceCommand command;
            IEnumerable<CommandPolyFencePoint> points = null;
            //fill the command
            switch (fence.FenceType)
            {
                case GeoFenceType.Circular:
                    if (enable)
                    {
                        CircularFence cFence = fence as CircularFence;
                        command = new EnableCircularFence
                        {
                            Account = GetDealerUser().Account,
                            Name = fence.Name.IsNullOrEmpty() ? "Fence " + fence.FenceIndex.ToString() : fence.Name,
                            CenterLatitude = cFence.CenterLatitude,
                            CenterLongitude = cFence.CenterLongitude,
                            CommandID = Guid.NewGuid(),
                            CommandStatus = CommandStatus.InProgress,
                            Device = veh.Device,
                            FenceIndex = index,
                            RadiusMeters = cFence.RadiusMeters,
                            TriggerType = cFence.TriggerType,
                            ReceivedTime = null,
                            SentTime = DateTime.UtcNow
                        };
                        iFence = new ITP.Common.Types.CircularGeofence
                        {
                            CenterPoint = new GPSPoint
                            {
                                Latitude = cFence.CenterLatitude,
                                Longitude = cFence.CenterLongitude
                            },
                            Enabled = true,
                            Index = index,
                            Name = cFence.Name.IsNullOrEmpty() ? "Fence " + index.ToString() : cFence.Name,
                            Radius = cFence.RadiusMeters,
                            Trigger = cFence.TriggerType.ToITP()
                        };
                    }
                    else
                    {
                        command = new DisableCircularFence
                        {
                            CommandID = Guid.NewGuid(),
                            CommandStatus = CommandStatus.InProgress,
                            Device = veh.Device,
                            FenceIndex = index,
                            ReceivedTime = null,
                            SentTime = DateTime.UtcNow
                        };
                    }
                    break;
                case GeoFenceType.Rectangular:
                    if (enable)
                    {
                        RectangularFence rFence = fence as RectangularFence;
                        command = new EnableRectangularFence
                        {
                            Account = GetDealerUser().Account,
                            Name = fence.Name.IsNullOrEmpty() ? "Fence " + fence.FenceIndex.ToString() : fence.Name,
                            CommandID = Guid.NewGuid(),
                            CommandStatus = CommandStatus.InProgress,
                            Device = veh.Device,
                            EastLongitude = rFence.EastLongitude,
                            FenceIndex = index,
                            NorthLatitude = rFence.NorthLatitude,
                            ReceivedTime = null,
                            SentTime = DateTime.UtcNow,
                            SouthLatitude = rFence.SouthLatitude,
                            WestLongitude = rFence.WestLongitude,
                            TriggerType = rFence.TriggerType
                        };
                        iFence = new PolygonGeoFence
                        {
                            Enabled = true,
                            Index = index,
                            Name = rFence.Name.IsNullOrEmpty() ? "Fence " + index.ToString() : rFence.Name,
                            Points = new List<GPSPoint>{
                                new GPSPoint{
                                    Latitude = rFence.NorthLatitude,
                                    Longitude = rFence.WestLongitude
                                },
                                new GPSPoint{
                                    Latitude = rFence.NorthLatitude,
                                    Longitude = rFence.EastLongitude
                                },
                                new GPSPoint{
                                    Latitude = rFence.SouthLatitude,
                                    Longitude = rFence.EastLongitude
                                },
                                new GPSPoint{
                                    Latitude = rFence.SouthLatitude,
                                    Longitude = rFence.WestLongitude
                                }
                            },
                            Trigger = rFence.TriggerType.ToITP()
                        };
                    }
                    else
                    {
                        command = new DisableRectangularFence
                        {
                            CommandID = Guid.NewGuid(),
                            CommandStatus = CommandStatus.InProgress,
                            Device = veh.Device,
                            FenceIndex = index,
                            ReceivedTime = null,
                            SentTime = DateTime.UtcNow
                        };
                    }
                    break;
                case GeoFenceType.Poly:
                    if (enable)
                    {
                        command = new EnablePolyFence
                        {
                            Account = GetDealerUser().Account,
                            Name = fence.Name.IsNullOrEmpty() ? "Fence " + fence.FenceIndex.ToString() : fence.Name,
                            CommandID = Guid.NewGuid(),
                            CommandStatus = CommandStatus.InProgress,
                            Device = veh.Device,
                            FenceIndex = index,
                            ReceivedTime = null,
                            SentTime = DateTime.UtcNow,
                            TriggerType = fence.TriggerType,
                        };
                        PolyFence pFence = fence as PolyFence;
                        points = pFence.Points.ToCommandPolyFencePoints((EnablePolyFence)command);
                        iFence = new PolygonGeoFence
                        {
                            Enabled = true,
                            Index = index,
                            Name = pFence.Name.IsNullOrEmpty() ? "Fence " + index.ToString() : pFence.Name,
                            Trigger = pFence.TriggerType.ToITP(),
                            Points = points.ToITPList()
                        };
                    }
                    else
                    {
                        command = new DisablePolyFence
                        {
                            CommandID = Guid.NewGuid(),
                            CommandStatus = CommandStatus.InProgress,
                            Device = veh.Device,
                            FenceIndex = index,
                            ReceivedTime = null,
                            SentTime = DateTime.UtcNow
                        };
                    }
                    break;
                default:
                    throw new FaultException<DeviceFault>(
                                    new DeviceFault()
                                    {
                                        FaultType = DeviceFaultType.UnsupportedFenceType
                                    }, "Geofence type is not supported.");
            }

            db.InsertOnSubmit(new GeofenceSetRequested
            {
                Vehicle = veh,
                TimeStamp = DateTime.UtcNow,
                User = db.ConsumerUsers.First(z => z.ID == UserID)
            });
            db.InsertNow(command);
            command.SendToDTS();

            db.InsertNow(new GeofenceSetRequested
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                TimeStamp = DateTime.UtcNow,
                OtherInfo = string.Format("SetGeofence({0},{1},{2}) called, result:{3}", vin, index, enable, command.CommandID.ToString())
            });

            return command.CommandID;
        }

        /// <summary>
        /// Sets the quick fence.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="enable">if set to <c>true</c> [enable].</param>
        /// <returns></returns>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.VehicleFault">VehicleFault</exception>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.GeneralFault">GeneralFault</exception>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public Guid SetQuickFence(string vin, bool enable)
        {
            //ValidateConsumerUser();

            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetQuickFence);

            //make sure it has a device installed
            veh.CheckForDevice();

            if (veh.IsQuickFenceSet == enable)
            {
                return COMPLETEDINLINE;
            }

            DeviceCommand command = null;

            if (enable)
            {
                command = new EnableETD();
            }
            else
            {
                command = new DisableETD();
            }

            command.Device = veh.Device;
            command.SentTime = DateTime.UtcNow;
            command.ReceivedTime = null;
            command.CommandStatus = CommandStatus.InProgress;

            db.InsertOnSubmit(new GeofenceSetRequested
            {
                User = db.ConsumerUsers.First(z => z.ID == UserID),
                TimeStamp = DateTime.UtcNow,
                Vehicle = veh
            });
            db.InsertNow(command);

            command.SendToDTS();
            db.InsertNow(new QuickFenceSetRequested
            {
                User = db.UserBases.Where(x => x.ID == UserID).Single(),
                Vehicle = veh,
                TimeStamp = DateTime.UtcNow,
                OtherInfo = string.Format("SetQuickFence({0},{1}) called, result:{2}", vin, enable, command.CommandID.ToString())
            });
            return command.CommandID;
        }

        /// <summary>
        /// Sets the speed alert.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <remarks>added auditing 5.5.11 pp</remarks>
        public Guid SetSpeedAlert(string vin, int value)
        {
            var veh = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetSpeedAlert);
            veh.CheckForDevice();

            var command = new SetOverspeed();
            command.CommandStatus = CommandStatus.InProgress;
            command.Device = veh.Device;
            command.SentTime = DateTime.UtcNow;
            command.Speed = value;

            db.InsertOnSubmit(new OverspeedAdjustmentRequested
            {
                TimeStamp = DateTime.UtcNow,
                User = db.ConsumerUsers.First(z => z.ID == UserID),
                Vehicle = veh,
                OtherInfo = string.Format("SetSpeedAlert({0},{1}) result:{2}", vin, value, command.CommandID.ToString())
            });
            db.InsertNow(command);

            command.SendToDTS();

            return command.CommandID;
        }

        public Guid StartTrackingSession(string vin, TimeSpan duration)
        {
            throw new FaultException<GeneralFault>(new GeneralFault
            {
                FaultType = GeneralFaultType.NotImplemented,
                AdditionalInfo = "This feature has not been implemented"
            }, "Tracking feature has not been implemented.");
        }

        /// <summary>
        /// Stores the geofence.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <param name="geofence">The geofence.</param>
        /// <returns></returns>
        /// <remarks>added auditing pp 5.5.11</remarks>
        /// <remark>added the hack to allow storing of disabled geofences.
        /// Send enabled fence, followed by disable fence.
        /// </remark>
        public Guid StoreGeofence(string vin, DataTypes.Geofence geofence)
        {
            var vehicle = GetConsumerVehicle(vin, ConsumerAuthorizations.CanSetGeoFence);

            bool disableAfterSet = false;

            vehicle.CheckForDevice();

            //ensure fence type is supported
            if (!vehicle.Device.SupportsFenceType(geofence))
            {
                throw new FaultException<DeviceFault>(
                                new DeviceFault
                                {
                                    FaultType = DeviceFaultType.UnsupportedFenceType,
                                    AdditionalInfo = "Fence type not supported"
                                }, "Fence type not supported."
                                );
            }

            //make sure fence is enabled
            if (!geofence.IsEnabled)
            {
                disableAfterSet = true;
            }

            //give it a name if it doesn't have one
            geofence.Name = geofence.Name.IsNullOrEmpty() ? "Fence " + geofence.Index.ToString() : geofence.Name;

            //translate the fence
            List<CommandPolyFencePoint> commandPoints = null;
            DeviceCommand databaseCommand = geofence.ToDb(vehicle.Device, db.ConsumerUsers.First(z => z.ID == UserID).Account, out commandPoints);

            //store fence command
            db.InsertOnSubmit(new GeofenceSetRequested
            {
                User = db.ConsumerUsers.First(z => z.ID == UserID),
                TimeStamp = DateTime.UtcNow,
                Vehicle = vehicle
            });
            db.InsertNow(databaseCommand);
            if ((commandPoints != null) && (commandPoints.Count > 0))
            {
                foreach (var point in commandPoints)
                {
                    point.Command = (EnablePolyFence)databaseCommand;
                    db.InsertOnSubmit(point);
                }
                db.SubmitChanges();
            }

            //send to DTS
            databaseCommand.SendToDTS();
            // ConsumerAuthorizations auth = ConsumerAuthorizations.NONE; why is this here?

            db.InsertNow(new GeofenceSetRequested
            {
                User = db.ConsumerUsers.FirstOrDefault(z => z.ID == UserID),
                TimeStamp = DateTime.UtcNow,
                Vehicle = vehicle,
                OtherInfo = string.Format("StoreGeofence({0},{1}) result: {2}", vin, geofence.Geometry.ToString(), databaseCommand.CommandID)
            });

            if (disableAfterSet)
            {
                return this.SetGeofence(vehicle.VIN, geofence.Index, false);
            }
            else
            {
                return databaseCommand.CommandID;
            }

            //pass back the database ID for further lookup
        }

        public void UpdateCurrentSkyLinkUser(DataTypes.SkyLinkUserInformation skyLinkUserInformation, string newpassword, string oldpassword)
        {
            #region not used in 'FindIT'

            //int? verified = 0;

            ////are they changing their password?
            //if (!newpassword.IsNullOrEmpty() && !oldpassword.IsNullOrEmpty())
            //{
            //    if (newpassword.Length < MIN_PASSWORD_LENGTH)
            //        throw C_VALIDATION_FAILURE;

            //    int result = db.VerifyPassword(UserID, null, oldpassword, ref verified);

            //    if (verified != 1)
            //    {
            //        throw C_VALIDATION_FAILURE;
            //    }
            //    //ok change the password
            //    if (db.ChangePassword(UserID, newpassword) == -1)
            //    {
            //        //shit it didn't work, tell them
            //        throw new FaultException<GeneralFault>(new GeneralFault
            //        {
            //            FaultType = GeneralFaultType.GeneralFault,
            //            AdditionalInfo = "User Password Change Failed, operation aborted"
            //        }, "User password change failed.");
            //    }
            //}

            //var user = GetConsumerUser();
            //user.AlternateEmail = skyLinkUserInformation.AlternateEmail;
            //user.CellPhone = skyLinkUserInformation.CellPhone;
            //user.CellPhoneProvider = FindCellPhoneProvider(skyLinkUserInformation.CellProvider);
            //user.FirstName = skyLinkUserInformation.FirstName;
            //user.LastName = skyLinkUserInformation.LastName;
            //user.LastUpdated = DateTime.UtcNow;
            //user.PrimaryPhone = skyLinkUserInformation.PrimaryPhone;
            //user.PIN = skyLinkUserInformation.  PIN;
            //user.SecretQuestionID = skyLinkUserInformation.SecretQuestionChoice;
            //if (!user.SecretQuestionAnswer.IsNullOrEmpty())
            //{
            //    user.SecretQuestionAnswer = skyLinkUserInformation.SecretQuestionAnswer;
            //}
            //user.Account.ApartmentNumber = skyLinkUserInformation.Suite;
            //user.Account.StreetAddress1 = skyLinkUserInformation.StreetAddress1;
            //user.Account.StreetAddress2 = skyLinkUserInformation.StreetAddress2;
            //user.Account.City = skyLinkUserInformation.City;
            //user.Account.State = skyLinkUserInformation.State;
            //user.Account.PostalCode = skyLinkUserInformation.PostalCode;
            //user.EmailNotifications = skyLinkUserInformation.EmailNotifications;
            //user.SMSNotifications = skyLinkUserInformation.SmsNotifications;
            //user.TimeZone = skyLinkUserInformation.TimeZone;
            //db.SubmitChanges();

            #endregion
        }

        public IEnumerable<SkyLinkWCFService.DataTypes.CreditCard> GetCCInfo()
        {
            foreach (var cc in GetDealerUser().Account.GetCreditCards())
            {
                yield return cc.ToWCF();
            }
        }

        public void UpdateCCInfo(BillingInfo billingInfo)
        {
            using (var dc = new VehicleManagement.Data.VehicleManagementDataContext())
            {
                Helper.UpdateCCInfo(this.UserAccountID, billingInfo, dc);
            }

        }

        public AllProducts GetCouponOfferings(string vin, string couponCode)
        {
            // This method is very similar to GetProduct Offerings with only subtle differences.  Because of
            // the similarities, it may make sense to look into combining them at some point.  However, the
            // decision was made to separate them since it is more efficient to query NetSuite a minimum number
            // of times and the set of general (non-coupon) product offerings will not change as coupons are
            // applied, thus making it optimal to query for coupon products separately.
            //
            // Note that this method does NOT return the products that this coupon applies to.  Instead it returns
            // products this product applies to that are valid for the vehicle against which the coupon
            // is being applied.
            //
            // This distinction means that if a user attempts to apply a PROTECT coupon to a vehicle that
            // was sold with ETD and the ETD plan is still current, it will return 0 products, since only 
            // PROTECT Upgrade or ETD Renewal products are valid on such vehicles.
            //
            // This is determined by limiting the plans that are returned to those that match the 
            // subscriptionPlanPeriods.PK_ID field - i.e. (p.periodPlan.ID == coupon.PlanId) in the
            // WHERE clause of the main select.

            if (!GetDealerUser().Authorization.HasFlag(ConsumerAuthorizations.CanManageSubscriptions))
            {
                throw C_INSUFFICIENT_PERMISSIONS;
            }
            var veh = GetConsumerVehicle(vin);

            CouponService.Response coupon = null;

            try
            {
                CouponService.CouponServiceClient client = new CouponService.CouponServiceClient();
                coupon = client.ValidateCoupon(couponCode);
            }
            catch (Exception e)
            {
                throw e;
            }

            /*****************************************************************************************************************
             * We are going to return the set of products that this vehicle is entitled to purchase.  To do that, we will 
             * check to see what plan the device was originally sold with and make our determination as follows:
             * 
             * 1) ETD renewals are only valid if the vehicle was sold with the ETD plan.
             * 2) ETD upgrades to PROTECT are only valid if an ETD plan is current on the vehicle, and then only for the same term length.
             * 3) MotoLink plans are only valid if the vehicle was sold with a MotoLink plan.
             * 4) Standard SkyLink plans are valid for all vehicles for which MotoLink and ETD PROTECT upgrade plans are not.
             * 
             * So, we will perform the above checks and set flags to indicate which plan types are valid.  Those flags will
             * then form an integral part of our query to pull the relevant plans which should be included in the response.
             * ***************************************************************************************************************/

            // ETD renewal plans are good if the vehicle was sold with ETD.
            bool getETDRenewals = (veh.PreSoldSubscriptionPlanPeriod != null) && (veh.PreSoldSubscriptionPlanPeriod.LinkSubscriptionPlan.Name.Contains("ETD"));

            // ETD upgrade plans are good only if the vehicle was sold with an ETD plan attached and it is still current.
            bool getETDUpgrades = getETDRenewals && veh.LinkSubscriptionMaps.Where(sub => (sub.PlanExpiration > DateTime.Now) && (sub.LinkSubscriptionPlan.Name.Contains("ETD"))).Count() > 0;
            int etdTermLength = 0;
            try
            {
                etdTermLength = veh.PreSoldSubscriptionPlanPeriod.LengthInDays;
            }
            catch
            {
                etdTermLength = 0;
            }

            // MotoLink plans are only good if we do have not had an ETD plan on the vehicle and the vehicle was sold with MotoLink.
            bool getMotoLink = ((getETDRenewals == false) &&
                               (veh.PreSoldSubscriptionPlanPeriod != null) && (veh.PreSoldSubscriptionPlanPeriod.LinkSubscriptionPlan.Name.Contains("MotoLink")));

            // SkyLink plans are good so long as we didn't sell the vehicle with a MotoLink plans attached.
            bool getSkyLink = ((getMotoLink == false) && (getETDUpgrades == false));


            //Get the relevant subscription plans
            // NOTE: The odd-looking where clause ensures only "PROTECT upgrades" for same term length as ETD are returned.
            var subProdItems1 = db.LinkSubscriptionPlans.Where(pac => pac.AccountType == VehicleManagement.Data.AccountType.ConsumerAccount)
                .Where(cap => (getMotoLink && cap.Name.Contains("MotoLink Advantage")) ||
                              (getSkyLink && cap.Name.Contains("SkyLink Advantage")) ||
                              (getETDRenewals && cap.Name.Contains("ETD renewal")) ||
                              (getETDUpgrades && cap.Name.Contains("ETD upgrade"))).
                Join(db.SubscriptionPlanPeriods, plan => plan.ID, periodPlan => periodPlan.LinkSubscriptionPlanID,
                (plan, periodPlan) => new { plan, periodPlan }).
                Where(p => (p.periodPlan.LengthInDays == etdTermLength || p.plan.Name.Contains("ETD") == false) && (p.periodPlan.ID == coupon.PlanId)).
                OrderBy(ob => ob.periodPlan.LengthInDays).ToList();
            var subProdItems = subProdItems1
                .Select(y => new
                {
                    item = y.periodPlan,
                    info = y.periodPlan.GetNetSuiteInfo()
                }).Select(pr => new Product
                {
                    Description = pr.info.Description,
                    ID = pr.item.ID,
                    ImageUrl = string.Empty,
                    Name = pr.info.Name,
                    OrdinalNumber = pr.item.LengthInDays,
                    Price = pr.info.Price,
                    Type = DataTypes.CartItemType.SubscriptionPlan
                }).ToList();

            //put the subscriptions into a product group
            ProductOptionGroup subscriptions = new ProductOptionGroup
            {
                GroupName = "Subscriptions",
                LineItems = subProdItems,
                IsListType = true
            };

            //everything we're going to try and sell the mark
            var ProductGroups = new AllProducts();

            var pog = new List<ProductOptionGroup>();
            pog.Add(subscriptions);

            ProductGroups.ProductGroups = pog;

            return ProductGroups;
        }

        public AllProducts GetProductOfferings(string vin)
        {
            if (!GetDealerUser().Authorization.HasFlag(ConsumerAuthorizations.CanManageSubscriptions))
            {
                throw C_INSUFFICIENT_PERMISSIONS;
            }

            var veh = GetConsumerVehicle(vin);
            var nsProdList = db.NonSubscriptionProducts.Where(x => x.Active == true &&
                    (veh.GetConsumerSubscriptionPlan().Contains(x.LinkSubscriptionPlan) ||
                     x.LinkSubscriptionPlan == null))
                    .Select(y => new Product
                    {
                        ID = y.ID,
                        Description = y.Description,
                        ImageUrl = y.ImgUrl,
                        Name = y.Name,
                        Price = y.Price,
                        Type = CartItemType.PromotionalItem
                    }).ToList();

            ProductOptionGroup nonSubscriptionProds = new ProductOptionGroup
            {
                GroupName = "Other Items",
                LineItems = nsProdList,
                IsListType = false
            };

            /*                  */

            /*****************************************************************************************************************
             * We are going to return the set of products that this vehicle is entitled to purchase.  To do that, we will 
             * check to see what plan the device was originally sold with and make our determination as follows:
             * 
             * 1) ETD renewals are only valid if the vehicle was sold with the ETD plan.
             * 2) ETD upgrades to PROTECT are only valid if an ETD plan is current on the vehicle, and then only for the same term length.
             * 3) MotoLink plans are only valid if the vehicle was sold with a MotoLink plan.
             * 4) Standard SkyLink plans are valid for all vehicles for which MotoLink and ETD PROTECT upgrade plans are not.
             * 
             * So, we will perform the above checks and set flags to indicate which plan types are valid.  Those flags will
             * then form an integral part of our query to pull the relevant plans which should be included in the response.
             * ***************************************************************************************************************/

            // ETD renewal plans are good if the vehicle was sold with ETD.
            bool getETDRenewals = (veh.PreSoldSubscriptionPlanPeriod != null) && (veh.PreSoldSubscriptionPlanPeriod.LinkSubscriptionPlan.Name.Contains("ETD"));

            // ETD upgrade plans are good only if the vehicle was sold with an ETD plan attached and it is still current.
            bool getETDUpgrades = getETDRenewals && veh.LinkSubscriptionMaps.Where(sub => (sub.PlanExpiration > DateTime.Now) && (sub.LinkSubscriptionPlan.Name.Contains("ETD"))).Count() > 0;
            int etdTermLength = 0;
            try
            {
                etdTermLength = veh.PreSoldSubscriptionPlanPeriod.LengthInDays;
            }
            catch
            {
                etdTermLength = 0;
            }

            // MotoLink plans are only good if we do have not had an ETD plan on the vehicle and the vehicle was sold with MotoLink.
            bool getMotoLink = ((getETDRenewals == false) &&
                               (veh.PreSoldSubscriptionPlanPeriod != null) && (veh.PreSoldSubscriptionPlanPeriod.LinkSubscriptionPlan.Name.Contains("MotoLink")));

            // SkyLink plans are good so long as we didn't sell the vehicle with a MotoLink plans attached.
            bool getSkyLink = ((getMotoLink == false) && (getETDUpgrades == false));


            //Get the relevant subscription plans
            var subProdItems1 = db.LinkSubscriptionPlans.Where(pac => pac.AccountType == VehicleManagement.Data.AccountType.ConsumerAccount)
                .Where(cap => (getMotoLink && cap.Name.Contains("MotoLink Advantage")) ||
                              (getSkyLink && cap.Name.Contains("SkyLink Advantage")) ||
                              (getETDRenewals && cap.Name.Contains("ETD renewal")) ||
                              (getETDUpgrades && cap.Name.Contains("ETD upgrade"))).
                Join(db.SubscriptionPlanPeriods, plan => plan.ID, periodPlan => periodPlan.LinkSubscriptionPlanID,
                (plan, periodPlan) => new { plan, periodPlan }).
                Where(p => p.periodPlan.LengthInDays == etdTermLength || p.plan.Name.Contains("ETD") == false).
                OrderBy(ob => ob.periodPlan.LengthInDays).ToList();

            // Now take the subscription plans and obtain the info we need from NetSuite,
            // while at the same time excluding promotional products (identified by a price of 0.00)
            var subProdItems = subProdItems1
                .Select(y => new
                {
                    item = y.periodPlan,
                    info = y.periodPlan.GetNetSuiteInfo()
                }).Select(pr => new Product
                {
                    Description = pr.info.Description,
                    ID = pr.item.ID,
                    ImageUrl = string.Empty,
                    Name = pr.info.Name,
                    OrdinalNumber = pr.item.LengthInDays,
                    Price = pr.info.Price,
                    Type = DataTypes.CartItemType.SubscriptionPlan
                }).Where(pr => pr.Price > 0).ToList();

            //put the subscriptions into a product group
            ProductOptionGroup subscriptions = new ProductOptionGroup
            {
                GroupName = "Subscriptions",
                LineItems = subProdItems,
                IsListType = true
            };

            //everything we're going to try and sell the mark
            var ProductGroups = new AllProducts();

            var pog = new List<ProductOptionGroup>();
            if (nonSubscriptionProds.LineItems.Count() != 0)
            {
                pog.Add(nonSubscriptionProds);
            }

            pog.Add(subscriptions);

            ProductGroups.ProductGroups = pog;

            return ProductGroups;
        }

        public PurchaseOrder SubmitCart(Cart cart)
        {
            CreateShoppingCart(cart);

            //var veh = GetConsumerVehicle(cart.VIN);

            return GetShoppingCart().GetPurchaseOrder();
        }

        public PurchaseReceipt SubmitOrder(BillingInfo billingInfo)
        {
            if (GetShoppingCart() == null)
            {
                throw C_INVALID_SHOPPING_CART;
            }

            var pr = GetShoppingCart().SubmitPurchase(billingInfo);

            return pr;
        }

        public bool IsCouponValid(string coupon)
        {
            var codes = ((string)ConfigurationManager.AppSettings["AdvantageCouponCode"]).Split(';').Select(c => c.ToLower()).AsEnumerable<string>();

            if (codes.Contains(coupon.ToLower()))
                return true;

            return false;
        }

        public void SubmitCustomerFeedBack(string email, string subject, string message)
        {
            FeedBack fb = new FeedBack();
            fb.UserID = UserID.Value;
            fb.Email = email;
            fb.Subject = subject;
            fb.Content = message;
            fb.NetSuiteCaseID = null;
            db.FeedBacks.InsertOnSubmit(fb);
            db.SubmitChanges();
        }

        public IEnumerable<SkyLinkWCFService.DataTypes.SecretQuestion> GetSecretQuestions()
        {
            var dat = db.SecretQuestions.Select(a => new DataTypes.SecretQuestion
            {
                ID = a.ID,
                Question = a.Question
            });

            return dat;
        }

        #region Utility methods

        /// <summary>
        /// Changes the password.
        /// Make sure your uid is valid
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="newpassword">The newpassword.</param>
        /// <returns></returns>
        private void ChangePassword(int userID, string newpassword)
        {
            if (db.ChangePassword(userID, newpassword) == -1)
            {
                //shit it didn't work, tell them
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.GeneralFault,
                    AdditionalInfo = "User Password Change Failed, operation aborted"
                }, "User password change failed.");
            }
        }

        private void ChangePasswordWithVerify(int userID, string oldpassword, string newpassword)
        {
            int? verified = 0;

            int result = db.VerifyPassword(userID, null, oldpassword, ref verified);

            if (verified != 1)
            {
                throw C_VALIDATION_FAILURE;
            }
            //ok change the password
            if (db.ChangePassword(UserID, newpassword) == -1)
            {
                //shit it didn't work, tell them
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.GeneralFault,
                    AdditionalInfo = "User Password Change Failed, operation aborted"
                }, "User password change failed.");
            }
        }

        private VehicleManagement.Data.CellPhoneProvider FindCellPhoneProvider(string provider)
        {
            return db.CellPhoneProviders.Where(x => x.Name.ToUpper() == provider.ToUpper()).FirstOrDefault();
        }

        private ConsumerUser GetConsumerUser_nope(int userID)
        {
            return db.ConsumerUsers.Where(user => user.ID == userID).FirstOrDefault();
        }

        /// <summary>
        /// Gets the current consumer user.
        /// </summary>
        /// <returns>ConsumerUser</returns>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.GeneralFault">If user not found, or is not a consumer user.</exception>
        private VehicleManagement.Data.ConsumerUser GetConsumerUser_nope()
        {
            var user = db.ConsumerUsers.Where(uid => uid.ID == UserID).FirstOrDefault();

            if (user == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault()
                {
                    FaultType = GeneralFaultType.UnknownUser,
                    AdditionalInfo = "User not in system"
                }, "User not in system.");
            }

            return user;
        }

        private VehicleManagement.Data.DealerUser GetDealerUser()
        {
            var user = db.DealerUsers.Where(uid => uid.ID == UserID).FirstOrDefault();

            if (user == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault()
                {
                    FaultType = GeneralFaultType.UnknownUser,
                    AdditionalInfo = "User not in system"
                }, "User not in system.");
            }

            return user;
        }

        private DealerUser GetDealerUser(int userID)
        {
            return db.DealerUsers.Where(user => user.ID == userID).FirstOrDefault();
        }

        private VehicleManagement.Data.Vehicle GetConsumerVehicle(string vin, ConsumerAuthorizations neededAuthorizations)
        {
            //var veh = db.UserVehicleMaps.OfType<ConsumerUserVehicleMap>().Where(map => map.UserBase.ID == UserID && map.Vehicle.VIN == vin)
            //        .Select(vehicle => new
            //        {
            //          vehicle.Vehicle,
            //          vehicle.Authorization
            //        }).FirstOrDefault();

            VehicleManagement.Data.Vehicle rVeh = GetDealerUser().Vehicles.Where(x => x.VIN == vin.Trim()).FirstOrDefault();

            var veh = new
            {
                rVeh,
                GetDealerUser().Authorization
            };

            if (veh == null)
            {
                throw C_VEHICLE_NOT_FOUND;
            }

            if (!veh.Authorization.HasFlag(neededAuthorizations))
            {
                throw new FaultException<GeneralFault>(new GeneralFault
                {
                    FaultType = GeneralFaultType.NotAuthorized,
                    AdditionalInfo = "User has insufficient privileges"
                }, "User has insufficient privileges.");
            }
            return veh.rVeh;
        }

        //hack added for FindIT
        private VehicleManagement.Data.Vehicle GetDealershipVehicle(string vin, DealerAuthorizations neededAuthorizations)
        {
            VehicleManagement.Data.Vehicle rVeh = GetDealerUser().Vehicles.Where(x => x.VIN == vin.Trim()).FirstOrDefault();

            var veh = new
            {
                rVeh,
                GetDealerUser().Authorization
            };

            if (veh == null)
            {
                throw C_VEHICLE_NOT_FOUND;
            }

            return veh.rVeh;
        }

        //hacked to return dealeruser vehicle
        private VehicleManagement.Data.Vehicle GetConsumerVehicle(string vin)
        {
            //var veh = db.UserVehicleMaps.OfType<ConsumerUserVehicleMap>().Where(map => map.UserBase.ID == UserID && map.Vehicle.VIN == vin)
            //        .Select(vehicle => vehicle.Vehicle)
            //        .FirstOrDefault();
            var veh = GetDealerUser().Vehicles.Where(x => x.VIN == vin.Trim()).FirstOrDefault();

            if (veh == null)
            {
                throw C_VEHICLE_NOT_FOUND;
            }

            return veh;
        }

        /// <summary>
        /// Gets the validated vehicle.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns>Vehicle</returns>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.VehicleFault">VehicleFault</exception>
        [Obsolete]
        private VehicleWithAuthorizations GetConsumerVehicleWithAuthorizations(string vin)
        {
            var veh = db.UserVehicleMaps.OfType<ConsumerUserVehicleMap>().Where(map => map.UserBase.ID == UserID && map.Vehicle.VIN == vin)
                            .Select(vehicle => new
                            {
                                vehicle.Vehicle,
                                vehicle.Authorization
                            })
                            .FirstOrDefault();

            if (veh == null)
            {
                throw C_VEHICLE_NOT_FOUND;
            }

            return new VehicleWithAuthorizations
            {
                Vehicle = veh.Vehicle,
                Authorizations = veh.Authorization
            };
        }

        /// <summary>
        /// Gets the vehicle plans. Regardless of type.
        /// </summary>
        /// <param name="veh">The Vehicle.</param>
        /// <returns>The subscription mappings or null if none.</returns>
        private IQueryable<LinkSubscriptionMap> GetVehiclePlans(VehicleManagement.Data.Vehicle vehicle)
        {
            return db.LinkSubscriptionMaps.Where(dbVIN => dbVIN.Vehicle.VIN == vehicle.VIN);
        }

        /// <summary>
        /// Gets the WCF vehicle.
        /// </summary>
        /// <param name="vin">The vin.</param>
        /// <returns>SkyLinkWCFService.DataTypes.Vehicle</returns>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.GeneralFault"></exception>
        private DataTypes.Vehicle GetWCFVehicle(string vin)
        {
            var dbVeh = GetConsumerVehicle(vin);
            var plans = db.LinkSubscriptionMaps
                            .Where(acnt => acnt.Account == GetDealerUser().Account && acnt.Vehicle.VIN == vin);

            if (plans == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault()
                {
                    FaultType = GeneralFaultType.Other,
                    AdditionalInfo = "The Vehicle does not have a plan associated with it, this shouldn't happen"
                }, "The vehicle does not have an appropriate plan associated with it.");
            }

            DataTypes.Vehicle veh = new DataTypes.Vehicle();

            veh.FillConsumerSetAttributes(dbVeh);

            List<SkyLinkWCFService.DataTypes.LinkSubscriptionPlan> wcfPlanList = new List<DataTypes.LinkSubscriptionPlan>();

            foreach (var plan in plans)
            {
                wcfPlanList.Add(
                        new SkyLinkWCFService.DataTypes.LinkSubscriptionPlan
                        {
                            Description = plan.LinkSubscriptionPlan.Description,
                            ExpirationDate = plan.PlanExpiration,
                            IsActive = plan.LinkSubscriptionPlan.IsActive,
                            Name = plan.LinkSubscriptionPlan.Name
                        });
            }

            veh.SkyLinkPlan = wcfPlanList.ToArray();

            return veh;
        }

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <exception cref="SkyLinkWCFService.DataTypes.Faults.GeneralFault">User is not in the system or is not a consumer user</exception>
        ///
        [Obsolete]
        private void ValidateConsumerUser()
        {
            var cUser = db.ConsumerUsers.Where(user => user.ID == UserID).FirstOrDefault();

            if (cUser == null)
            {
                throw new FaultException<GeneralFault>(new GeneralFault()
                {
                    FaultType = GeneralFaultType.UnknownUser,
                    AdditionalInfo = "User not in system"
                }, "User not in system.");
            }
        }

        #endregion Utility methods

        #region Static Data Methods

        /// <summary>
        /// Builds a list of states.
        /// </summary>
        /// <returns>A list of State objects</returns>
        /// <remarks>This should eventually come from the database.</remarks>
        private static List<State> BuildStateList()
        {
            int id = 1; // state id seed
            return new List<State>
            {
                new State(id++, "AL", "Alabama"),
                new State(id++, "AK", "Alaska"),
                new State(id++, "AS", "American Samoa"),
                new State(id++, "AZ", "Arizona"),
                new State(id++, "AR", "Arkansas"),
                new State(id++, "CA", "California"),
                new State(id++, "CO", "Colorado"),
                new State(id++, "CT", "Connecticut"),
                new State(id++, "DE", "Delaware"),
                new State(id++, "DC", "District of Columbia"),
                new State(id++, "FM", "Federated States of Micronesia"),
                new State(id++, "FL", "Florida"),
                new State(id++, "GA", "Georgia"),
                new State(id++, "HI", "Hawaii"),
                new State(id++, "ID", "Idaho"),
                new State(id++, "IL", "Illinois"),
                new State(id++, "IN", "Indiana"),
                new State(id++, "IA", "Iowa"),
                new State(id++, "KS", "Kansas"),
                new State(id++, "KY", "Kentucky"),
                new State(id++, "LA", "Louisiana"),
                new State(id++, "ME", "Maine"),
                new State(id++, "MH", "Marshall Islands"),
                new State(id++, "MD", "Maryland"),
                new State(id++, "MA", "Massachusetts"),
                new State(id++, "MI", "Michigan"),
                new State(id++, "MN", "Minnesota"),
                new State(id++, "MS", "Mississippi"),
                new State(id++, "MO", "Missouri"),
                new State(id++, "MT", "Montana"),
                new State(id++, "NE", "Nebraska"),
                new State(id++, "NV", "Nevada"),
                new State(id++, "NH", "New Hampshire"),
                new State(id++, "NJ", "New Jersey"),
                new State(id++, "NM", "New Mexico"),
                new State(id++, "NY", "New York"),
                new State(id++, "NC", "North Carolina"),
                new State(id++, "ND", "North Dakota"),
                new State(id++, "OH", "Ohio"),
                new State(id++, "OK", "Oklahoma"),
                new State(id++, "OR", "Oregon"),
                new State(id++, "PA", "Pennsylvania"),
                new State(id++, "PR", "Puerto Rico"),
                new State(id++, "RI", "Rhode Island"),
                new State(id++, "SC", "South Carolina"),
                new State(id++, "SD", "South Dakota"),
                new State(id++, "TN", "Tennessee"),
                new State(id++, "TX", "Texas"),
                new State(id++, "UT", "Utah"),
                new State(id++, "VT", "Vermont"),
                new State(id++, "VI", "Virgin Islands"),
                new State(id++, "VA", "Virginia"),
                new State(id++, "WA", "Washington"),
                new State(id++, "WV", "West Virginia"),
                new State(id++, "WI", "Wisconsin"),
                new State(id++, "WY", "Wyoming")
            };
        }

        #endregion

        #region Nested Types

        [Obsolete]
        public class VehicleWithAuthorizations
        {
            #region Properties

            public ConsumerAuthorizations Authorizations
            {
                get;
                set;
            }

            public VehicleManagement.Data.Vehicle Vehicle
            {
                get;
                set;
            }

            #endregion Properties
        }

        #endregion Nested Types
    }
}