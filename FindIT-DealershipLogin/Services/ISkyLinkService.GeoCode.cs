﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using SkyLinkWCFService.DataTypes;

namespace SkyLinkWCFService.Services
{
	
	public partial interface ISkyLinkService
	{
		[OperationContract]
		LocationPoint GeoCode(string address, string city, string state, string country);

		[OperationContract]
		LocationPoint ReverseGeoCode(GeoPoint geoPoint);
	}
}
