﻿namespace SkyLinkWCFService.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Web;

    using SkyLinkWCFService.DataTypes;
    using SkyLinkWCFService.DataTypes.Faults;

    public partial interface ISkyLinkService
    {
        #region Methods

        //hacky hack added for must have FindIT Features
        [OperationContract]
        Dictionary<string, GeoPoint> GetAccessableDealerships();

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        DashBoardInfo GetDashboard(string vin);

        [OperationContract]
        [FaultContract(typeof(VehicleFault))]
        [FaultContract(typeof(SessionFault))]
        Trip GetMostRecentTrip(string vin);

        [OperationContract]
        [FaultContract(typeof(VehicleFault))]
        [FaultContract(typeof(SessionFault))]
        TripInfo GetTrip(string vin, DateTime startDate);

        [OperationContract]
        [FaultContract(typeof(VehicleFault))]
        [FaultContract(typeof(GeneralFault))]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<TripSummary> GetTripsSummaries(string vin, DateTime startDate, DateTime endDate,
            int maxReports);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<Vehicle> GetVehicles();

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<Vehicle> GetVehicleByVIN(string vin);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<Vehicle> GetVehiclesByVINSuffix(string vinSuffix, int userType);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<Vehicle> GetFindITVehicles(string searchTerm);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<VehicleMaintenanceInterval> GetVehicleMaintenanceIntervals(string vin);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<DataTypes.VehicleMaintenanceInterval> UpdateVehicleMaintenanceIntervals(string vin, int?[] maintenanceIntervalID);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        bool LogVehicleMaintenanceIntervals(string vin, int[] intervalIndexes);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<MaintenanceInterval> GetMaintenanceIntervals();

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        bool UpdateMaintenanceIntervals(SkyLinkWCFService.DataTypes.MaintenanceInterval[] intervals);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        bool DeleteMaintenanceIntervals(int[] intervalIDs);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IEnumerable<MaintenanceInterval> AddMaintenanceIntervals(SkyLinkWCFService.DataTypes.MaintenanceInterval[] intervals);
        
        #endregion Methods
    }
}