﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.SessionState;
using SkyLinkWCFService.DataTypes;
using SkyLinkWCFService.Repositories;
using SkyLinkWCFService.Services;

namespace SkyLinkWCFService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SkyLinkService" in code, svc and config file together.
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, InstanceContextMode = InstanceContextMode.PerSession)]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class SkyLinkService : ISkyLinkService
	{
		#region Fields

		RepositoryLive data;

		#endregion Fields

		#region Constructors

		public SkyLinkService()
		{
			//TODO: remove this hack
			HttpSessionState session = null;

			if (HttpContext.Current != null)
			{
				session = HttpContext.Current.Session;
                if (session.Keys.OfType<string>().FirstOrDefault(z => z == "AltUserID") != null)
                {
                    // Only log in as alternate user if AltUserID is present and is non-zero.
                    if ((int)session["AltUserID"] > 0)
                    {
                        data = new RepositoryLive((int)session["AltUserID"]);
                        return;
                    }
                }
                if (session.Keys.OfType<string>().FirstOrDefault(z => z == "UserID") != null)
				{
					data = new RepositoryLive((int)session["UserID"]);
					return;
				}
            }

#if DEBUG && TESTING
            data = new RepositoryLive("findit@drivetime.com", "inilex");
//            data = new RepositoryLive("c_paul.kpeterson@gmail.com", "inilex");
			//data = new RepositoryLive("cj_morton@hotmail.com", "inilex");

			Console.WriteLine("In Debug Mode");
#endif

            //data = new RepositoryLive("preeder@inilex.com", "inilex");
            //data = new RepositoryLive("godonnell@inilex.com", "inilex");
            
            //data = new RepositoryLive("chrishill4u@hotmail.com", "inilex");
			//data = new RepositoryLive("chrishill4u@hotmail.com", "inilex");
		}

		public SkyLinkService(RepositoryLive repository)
		{
			data = repository;
		}

		#endregion Constructors

        //Hack to support FindIT for everyone.
        public Dictionary<string, GeoPoint> GetAccessableDealerships()
        {
            return data.GetAccessableDealerships();
        }

		public int ClearAllFencingViolations(string vin)
		{
			data.ClearAllFencingViolations(vin);
			return 0;
		}

		public int ClearAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			data.ClearAllFencingViolationsByDate(vin, startDate, endDate);
			return 0;
		}

		public void ClearAllSpeedAlarms(string vin)
		{
			data.ClearAllSpeedAlarms(vin);
		}

		public void ClearAllVoltageAlarms(string vin)
		{
			data.ClearAllVoltageAlarms(vin);
		}

		public void ClearFencingViolation(int id)
		{
			data.ClearFencingViolation(id);
			return;
		}

		public void ClearSpeedAlarm(int id)
		{
			data.ClearSpeedAlarm(id);
		}

		public void ClearSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			data.ClearSpeedAlarmsByDate(vin, startDate, endDate);
		}

		public void ClearVoltageAlarm(int id)
		{
			data.ClearVoltageAlarm(id);
		}

		public void ClearVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			data.ClearVoltageAlarmsByDate(vin, startDate, endDate);
		}

		public int CreateSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newPassword)
		{
			return data.CreateSkyLinkUser(skyLinkUserInformation, newPassword);
		}

		public Guid EnableSpeedAlert(string vin, bool enable)
		{
			return data.EnableSpeedAlert(vin, enable);
		}

		public void EndTrackingSession(string vin)
		{
			data.EndTrackingSession(vin);
		}

		public IEnumerable<LocationPoint> GetAllFencingViolations(string vin)
		{
			return data.GetAllFencingViolations(vin);
		}

		public IEnumerable<LocationPoint> GetAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			return data.GetAllFencingViolationsByDate(vin, startDate, endDate);
		}

        public VehicleEventsResponse GetEvents(VehicleFilter vFilter, EventFilter eFilter, Boolean lastLocation, int userType=0)
        {
            return data.GetEvents(vFilter, eFilter, lastLocation, userType);
        }

		public IEnumerable<LocationPoint> GetAllSpeedAlarms(string vin)
		{
			return data.GetAllSpeedAlarms(vin);
		}

		public IEnumerable<LocationPoint> GetAllSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			return data.GetAllSpeedAlarmsByDate(vin, startDate, endDate);
		}

		public IDictionary<int, SkyLinkUserInformation> GetAllUsers()
		{
			return data.GetAllUsers();
		}

		public IEnumerable<LocationPoint> GetAllVoltageAlarms(string vin)
		{
			return data.GetAllVoltageAlarms(vin);
		}

		public IEnumerable<LocationPoint> GetAllVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			return data.GetAllVoltageAlarmsByDate(vin, startDate, endDate);
		}

		public float? GetBatteryVoltageAlarmLevel(string vin)
		{
			return data.GetBatteryVoltageAlarmLevel(vin);
		}

		public IEnumerable<string> GetCellPhoneProviders()
		{
			return data.GetCellPhoneProviders();
		}

		public ECommandStatus GetCommandStatus(Guid commandId)
		{
			return data.GetCommandStatus(commandId);
		}

		public IEnumerable<ConsumerFaq> GetConsumerFaqs()
		{
			return data.GetConsumerFaqs();
		}

		public SkyLinkUserInformation GetCustomerInfo()
		{
			return data.GetCustomerInfo();
		}

		public DashBoardInfo GetDashboard(string vin)
		{
			return data.GetDashboard(vin);
		}

		public IEnumerable<Geofence> GetGeofences(string vin)
		{
			return data.GetGeofences(vin);
		}

        public IEnumerable<MaintenanceInterval> GetMaintenanceIntervals()
        {
			throw new NotImplementedException();
            //return data.GetMaintenanceIntervals();
        }

        public bool UpdateMaintenanceIntervals(SkyLinkWCFService.DataTypes.MaintenanceInterval[] intervals)
        {
			throw new NotImplementedException();
            //return data.UpdateMaintenanceIntervals(intervals);
        }

        public bool DeleteMaintenanceIntervals(int[] intervalIDs)
        {
            return data.DeleteMaintenanceIntervals(intervalIDs);
        }

        public IEnumerable<MaintenanceInterval> AddMaintenanceIntervals(SkyLinkWCFService.DataTypes.MaintenanceInterval[] intervals)
        {
			throw new NotImplementedException();
            //return data.AddMaintenanceIntervals(intervals);
        }

        public IEnumerable<VehicleMaintenanceInterval> GetVehicleMaintenanceIntervals(string vin)
        {
            return data.GetVehicleMaintenanceIntervals(vin);
        }

        public IEnumerable<DataTypes.VehicleMaintenanceInterval> UpdateVehicleMaintenanceIntervals(string vin, int?[] maintenanceIntervalID)
        {
			throw new NotImplementedException();
            //return data.UpdateVehicleMaintenanceIntervals(vin, maintenanceIntervalID);
        }

        public bool LogVehicleMaintenanceIntervals(string vin, int[] intervalIndexes)
        {
			throw new NotImplementedException();
            //return data.LogVehicleMaintenanceIntervals(vin, intervalIndexes);
        }

        public LocationPoint GetLastLocation(string vin)
		{
			return data.GetLastLocation(vin);
		}

		public IEnumerable<LocationPoint> GetLastNLocations(string vin, int numberOfLocations)
		{
			return data.GetLastNLocations(vin, numberOfLocations);
		}

		public IEnumerable<LocationPoint> GetLocationsAfterId(string vin, int locationId)
		{
			return data.GetLocationsAfterId(vin, locationId);
		}

		public IEnumerable<LocationPoint> GetLocationsByDate(string vin, DateTime startDate, DateTime endDate)
		{
			return data.GetLocationsByDate(vin, startDate, endDate);
		}

		public IEnumerable<LocationPoint> GetLocationsSince(string vin, DateTime startDate)
		{
			return data.GetLocationsSince(vin, startDate);
		}

		public Trip GetMostRecentTrip(string vin)
		{
			return data.GetMostRecentTrip(vin);
		}

		public int GetNumberOfGeofencesSupported(string vin)
		{
			return data.GetNumberOfGeofencesSupported(vin);
		}

		public int? GetSpeedAlertSetting(string vin)
		{
			return data.GetSpeedAlertSetting(vin);
		}

        public IEnumerable<State> GetStates()
        {
            return data.GetStates();
        }

		public IEnumerable<EFenceGeometery> GetSupportedGeofenceTypes(string vin)
		{
			return data.GetSupportedGeofenceTypes(vin);
		}

		public TripInfo GetTrip(string vin, DateTime startDate)
		{
			return data.GetTrip(vin, startDate);
		}

		public IEnumerable<TripSummary> GetTripsSummaries(string vin, DateTime startDate, DateTime endDate, int maxReports)
		{
			return data.GetTripsSummaries(vin, startDate, endDate, maxReports);
		}

		public IEnumerable<Vehicle> GetVehicles()
		{
			return data.GetVehicles();
		}

        public IEnumerable<Vehicle> GetVehicleByVIN(string vin)
        {
            return data.GetVehicleByVIN(vin);
        }

        public IEnumerable<Vehicle> GetVehiclesByVINSuffix(string vinSuffix, int userType)
        {
            return data.GetVehiclesByVINSuffix(vinSuffix, userType);
        }

        public IEnumerable<Vehicle> GetFindITVehicles(string searchTerm)
        {
            return data.GetFindITVehicles(searchTerm);
        }

        public LocationPoint GetMostRecentLocationByType(string vin, LocationAlarmType locationType)
		{
			return data.GetMostRecentLocationByType(vin, locationType);
		}

		public bool IsQuickFenceSet(string vin)
		{
			return data.IsQuickFenceSet(vin);
		}

		public bool IsSpeedAlertEnabled(string vin)
		{
			return data.IsSpeedAlertEnabled(vin);
		}

		public Guid RemoveGeofence(string vin, int fenceId)
		{
			return data.RemoveGeofence(vin, fenceId);
		}

		public Guid RequestLocationUpdate(string vin)
		{
			return data.RequestLocationUpdate(vin);
		}

		public Guid SetBatteryVoltageAlarmLevel(string vin, float level)
		{
			return data.SetBatteryVoltageAlarmLevel(vin, level);
		}

		public Guid SetGeofence(string vin, int index, bool enable)
		{
			return data.SetGeofence(vin, index, enable);
		}

		public Guid SetQuickFence(string vin, bool enable)
		{
			return data.SetQuickFence(vin, enable);
		}

		public Guid SetSpeedAlert(string vin, int value)
		{
			return data.SetSpeedAlert(vin, value);
		}

		public Guid StartTrackingSession(string vin, TimeSpan duration)
		{
			return data.StartTrackingSession(vin, duration);
		}

		public Guid StoreGeofence(string vin, Geofence geofence)
		{
			return data.StoreGeofence(vin, geofence);
		}

		public void UpdateCurrentSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newpassword, string oldpassword)
		{
			data.UpdateCurrentSkyLinkUser(skyLinkUserInformation, newpassword, oldpassword);
		}

		public bool IsBatteryAlertEnabled(string vin)
		{
			return data.IsBatteryAlertEnabled(vin);
		}

		public Guid SetBatteryAlertState(string vin, bool state)
		{
			return data.SetBatteryAlertState(vin, state);
		}

        public IEnumerable<CreditCard> GetCCInfo()
        {
            return data.GetCCInfo();
        }

        public void UpdateCCInfo(BillingInfo billingInfo)
        {
            data.UpdateCCInfo(billingInfo);
        }

        public AllProducts GetCouponOfferings(string vin, string couponCode)
        {
            return data.GetCouponOfferings(vin, couponCode);
        }

        public AllProducts GetProductOfferings(string vin)
		{
			return data.GetProductOfferings(vin);
		}

		public PurchaseOrder SubmitCart(Cart cart)
		{
			return data.SubmitCart(cart);
		}

		public PurchaseReceipt SubmitOrder(BillingInfo order)
		{
			return data.SubmitOrder(order);
		}

		public bool IsCouponValid(string coupon)
		{
			return data.IsCouponValid(coupon);
		}

		public void SubmitCustomerFeedBack(string email, string subject, string message)
		{
			data.SubmitCustomerFeedBack(email, subject, message);
		}

		public IEnumerable<SecretQuestion> GetSecretQuestions()
		{
			return data.GetSecretQuestions();
		}

		public LocationPoint GeoCode(string address, string city, string state, string country)
		{
			return data.GeoCode(address, city, state, country);
		}

		public LocationPoint ReverseGeoCode(GeoPoint geoPoint)
		{
			return data.ReverseGeoCode(geoPoint);
		}
	}
}