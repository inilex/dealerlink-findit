﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    public class DealerShip
    {
        /// <summary>
        /// The Id of the Dealership
        /// </summary>
        /// <remarks>added 02Aug11, for dealer banner retrieval</remarks>
        [DataMember]
        public int DealerId;
        /// <summary>
        /// Company Name
        /// </summary>
        [DataMember]
        public string CompanyName;
        /// <summary>
        /// Street Address
        /// </summary>
        [DataMember]
        public string StreetAddress1;
        /// <summary>
        /// Street Address 2
        /// </summary>
        [DataMember]
        public string StreetAddress2;
        /// <summary>
        /// State
        /// </summary>
        [DataMember]
        public string State;
        /// <summary>
        /// Suite
        /// </summary>
        [DataMember]
        public string Suite;
        /// <summary>
        /// City
        /// </summary>
        [DataMember]
        public string City;
        /// <summary>
        /// Postal Code
        /// </summary>
        [DataMember]
        public string PostalCode;
        /// <summary>
        /// Phone Number
        /// </summary>
        [DataMember]
        public string PhoneNumber;

        /// <summary>
        /// The dealerships website url.
        /// </summary>
        [DataMember]
        public string URL;


    }
}