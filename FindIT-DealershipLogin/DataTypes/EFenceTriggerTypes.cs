﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    public enum EFenceTriggerTypes
    {
        [EnumMember]
        Enter = 0,
        [EnumMember]
        Exit = 1,
        [EnumMember]
        Transition = 2

    }
}