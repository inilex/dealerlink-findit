﻿using System;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// Subscription plan for a vehicle
	/// </summary>
	[DataContract(Namespace = "http://mysky-link.com/skylink")]
	public class LinkSubscriptionPlan
	{
		/// <summary>
		/// Name of the plan
		/// </summary>
		[DataMember]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Human description of the plan
		/// </summary>
		[DataMember]
		public string Description
		{
			get;
			set;
		}

		/// <summary>
		/// Whether or not the plan is active
		/// </summary>
		[DataMember]
		public bool IsActive
		{
			get;
			set;
		}

		/// <summary>
		/// When the plan expires
		/// </summary>
		[DataMember]
		public DateTime ExpirationDate
		{
			get;
			set;
		}
	}
}