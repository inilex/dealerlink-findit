﻿using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	[DataContract]
	public class ConsumerFaq
	{
		[DataMember]
		public string Category
		{
			get; set;
		}
		[DataMember]
		public string Question
		{
			get; set;
		}
		[DataMember]
		public string Answer
		{
			get; set;
		}
	}
}
