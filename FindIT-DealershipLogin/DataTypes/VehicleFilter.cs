﻿using System;
using System.Runtime.Serialization;


namespace SkyLinkWCFService.DataTypes
{
    [DataContract(Namespace = "http://mysky-link.com/skylink")]
    public class VehicleFilter
    {
        public VehicleFilter()
        {
        }

        [DataMember]
        public string[] vins { get; set; }

        [DataMember]
        public int maxCount { get; set; }
        
        [DataMember]
        public Area area{ get; set; }

    }
}