﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	[DataContract]
	public class SecretQuestion
	{
		[DataMember]
		public int ID
		{
			get;
			set;
		}

		[DataMember]
		public string Question
		{
			get;
			set;
		}
	}
}
