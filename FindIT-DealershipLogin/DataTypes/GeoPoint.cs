﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    [KnownType(typeof(LocationPoint))]
    public class GeoPoint
    {
        public GeoPoint()
        {
        }

        public GeoPoint(double lat, double lon)
        {
            this.Lat = lat;
            this.Lon = lon;
        }

        [DataMember]
        public double Lat { get; set; }

        [DataMember]
        public double Lon { get; set; }
    }
}