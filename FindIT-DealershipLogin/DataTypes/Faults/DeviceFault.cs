﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes.Faults
{
	[DataContract]
	public class DeviceFault
	{
		[DataMember]
		public DeviceFaultType FaultType { get; set; }

		[DataMember]
		public string AdditionalInfo { get; set; }
	}

	[DataContract]
	public enum DeviceFaultType
	{
		[EnumMember]
		GeofenceIndexOutOfRange = 1,

		[EnumMember]
		UnsupportedFenceType = 2,

		[EnumMember]
		GeofenceDoesNotExist = 3
	}
}