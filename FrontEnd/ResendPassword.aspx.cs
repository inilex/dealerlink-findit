﻿using SkyLink;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VehicleManagement.Data;

namespace FrontEnd
{
    public partial class ResendPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //		document.getElementById('btnHome.visibility = "hidden";
        }

        protected void btnSubmit_Click(Object sender, EventArgs e)
        {
            //verify their email address exists.
            using (var db = new VehicleManagementDataContext())
            {
                String email = txtEmail.Text.Trim();

                var user = db.DealerUsers.Where(x => x.PrimaryEmail == email).FirstOrDefault();

                if (user == null)
                {
                    errorContentArea.InnerHtml = "Unknown User";

                    return;
                }

                if ((user.Account == null) || (user.Account.Vehicles.FirstOrDefault()) == null)
                {
                    errorContentArea.InnerHtml = "This account has not been activated yet.<br> Please follow this  <a href=\"http://www.mysky-link.com/NewSubscriberLogin.aspx\">link</a>, and resubmit your vehicles information.";
                    return;

                }

                String newPassword = SkylinkUtil.RandomString(15);

                try
                {

                    db.ChangePassword(user.ID, newPassword);
                }
                catch (Exception)
                {
                    Response.Redirect("error.html");
                }

                //errorContentArea.InnerHtml = newPassword;

                MailMessage theMessage = new MailMessage();
                theMessage.Subject = "SkyLink account updated";
                theMessage.Priority = MailPriority.Normal;

                theMessage.To.Add(new MailAddress(email));
                theMessage.Bcc.Add(new MailAddress("SkyLinkActivations@inilex.com"));

                theMessage.Body = Environment.NewLine;
                theMessage.Body += String.Format("As requested, your SkyLink password has been changed.") + Environment.NewLine + Environment.NewLine;
                theMessage.Body += String.Format("Your new password is: {0}", newPassword) + Environment.NewLine + Environment.NewLine;
                theMessage.Body += String.Format("It is strongly recommended you change your password as soon as possible.") + Environment.NewLine + Environment.NewLine;
                theMessage.Body += "http://www.MySky-Link.com/CurrentSubscriberLogin.aspx" + Environment.NewLine + Environment.NewLine + Environment.NewLine;

                theMessage.Body += Environment.NewLine + Environment.NewLine;
                theMessage.Body += "Sincerly your SkyLink support team";

                SmtpClient smtp = new SmtpClient();

                smtp.Send(theMessage);

                errorContentArea.InnerHtml = "A new password has been sent to your email address.";
                txtEmail.Enabled = false;
                txtEmail.Text = "";
                btnSubmit.Enabled = false;
            }

        }
    }
}