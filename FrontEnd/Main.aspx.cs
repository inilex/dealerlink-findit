﻿using System;
using System.Linq;
using System.Web.UI;
using VehicleManagement.Data;

namespace FrontEnd
{
    public partial class Main : System.Web.UI.Page
    {
        #region Properties (3)
        String VIN
        {
            get
            {
                return (String)ViewState["vVIN"];
            }
            set
            {
                ViewState["vVIN"] = value;
            }
        }
        Boolean isInitialLogin
        {
            get
            {
                return (Boolean)ViewState["isInitialLogin"];
            }

            set
            {
                ViewState["isInitialLogin"] = value;
            }
        }


        #endregion

        #region Methods (1)

        protected void SetError(String message)
        {
            txtPwd.Text = String.Empty;
            lblError.Text = message;
        }

        #endregion

        #region Event Handlers (3)


        /// <summary>
        /// v1.01 removed encryption
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                String email = Request.QueryString["email"];
                String vin = Request.QueryString["vin"];
                String pwd = null;

                System.Web.HttpCookie tmpEmail = Request.Cookies["userName"];
                if (tmpEmail != null)
                {
                    txtEmail.Text = Server.HtmlEncode(tmpEmail.Value);
                }

                // stick the vin in the viewstate
                VIN = vin;

                isInitialLogin = false;
            }
        }


        /// <summary>
        /// Logins the user in
        /// 1. if their a new user
        ///     a. Update their SkyLink info, accepted terms ect.
        ///     b. Associate their Skylink account with the Vehicle.
        ///     c. Send them to the update info page.
        /// 2. if their a returning user adding another vehicle
        ///     a. update the vehicle record with their id
        ///     b. send them to the update page.
        /// 3. if their just logging in, log them in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void robLogin_Click(Object sender, EventArgs e)
        {
            String email = txtEmail.Text.Trim();
            String pwd = String.Empty;

            if (ViewState["vsPWD"] != null)
            {
                pwd = (String)ViewState["vsPWD"];
            }
            else
            {
                pwd = txtPwd.Text.Trim();
            }


            //int uid = SkylinkUtil.Login_SkyLinkUser(email, pwd);

            //if (uid == 0)
            //{
            //    // we don't know who the hell you are, you're not in our db
            //    SetError(String.Format("Login failed"));
            //    return;
            //}

            using (var db = new VehicleManagementDataContext())
            {
                int? userID = null;
                db.LoginUser(email, pwd, ref userID);
                if (userID == null || userID < 1)
                {
                    // we don't know who the hell you are, your not in our db
                    SetError("Login failed");
                    return;
                }
                DealerUser slu = db.DealerUsers.FirstOrDefault(x => x.PrimaryEmail == email);
                Vehicle vehicle = null;

                // shouldn't happen, but why not check
                if (slu == null)
                {
                    SetError("Login failed.");
                    return;
                }

                ViewState.Clear();
                Session["UserID"] = userID;
                Response.Cookies["userName"].Value = email;
                Response.Cookies["userName"].Expires = DateTime.Now.AddDays(365);
                Response.Redirect("index.html");
            }
        }

        #endregion
    }
}