﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResendPassword.aspx.cs" Inherits="FrontEnd.ResendPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SkyLINK</title>
    <link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="mobilePROTECT.css" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
	
    <script type="text/javascript" src="jquery.js"></script> 
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
</head>

<body>

<script type="text/javascript">
    $(document).ready(function () {
        document.getElementById("btnHome").style.visibility = "visible";
        document.getElementById("btnHome").onclick = function () { window.history.back(); };
    });
 </script>



   	<!--#include file="banner.inc" -->
    <form id="form1" runat="server">
    <div id="container">
            <div id="SLBotRt" style="font-size:75%">
                Enter your information below.<br/>A temporary password will be emailed to you.<br />
            </div>
        </div>
        <br/>
            <div id="CenterContent">
                <div class="row errorContent" id="errorContentArea" runat="server" style="font-size:75%; color:red" >
                <asp:RequiredFieldValidator runat="server" ID="rfvEmail" ControlToValidate="txtEmail"
                    InitialValue="" ErrorMessage="Email Address Required!" Display="Dynamic" />
                <asp:RegularExpressionValidator runat="server" ID="regExpValEmail" ErrorMessage="Email Format is Incorrect"
                    ControlToValidate="txtEmail" ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"
                    Display="Dynamic" />
                </div>
                <div class="row" align="center">
                    <div class="label">
                        Email Address: <asp:TextBox runat="server" ID="txtEmail" Columns="20" />
                        </div>
                </div>
                <div class="row" align="center">
                  <asp:ImageButton ImageUrl="images/btn_submit.png" runat="server" ID="btnSubmit" OnClick="btnSubmit_Click"/>
                </div>
            
            <br clear="all" />
        </div>
        <div id="SLMidBotBan" style="font-size:75%">
            If you have questions please call <br/>
            <span style='font-weight: bold;'>TOLL FREE 1-877-600-6101</span></div>
      </div>
    </form>
</body>
</html>
