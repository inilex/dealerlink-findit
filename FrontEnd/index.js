//GLOBAL CONSTANTS ----------------------------------------------------------
var autoLocationCheckPeriod = 15 * 60 *1000;	// Check for updated locations for each vehicle every 10 seconds
var locationUpdatePeriod = 2500; 				// Frequency with which to check for updated locations following a request from user
var locationUpdateIterations = 15;				// Number of times to check for location update to arrive before timing out

var autoFenceCheckPeriod = 15 * 60 * 1000;		// Update geofence status for each vehicle every 10 seconds
var fenceStatusUpdatePeriod = 2500;				// Frequency with which to check fence status toggle took effect following a request from user
var fenceStatusUpdateIterations = 15;			// Number of times to check for fence status toggle to take effect before timing out

var ScreenLayouts = {landscape:0, portait:1};	// Valid screen layout configurations
var minLandscapeWidth = 400;					// Width below which layout is considered portrait


//GLOBAL VARIABLES-----------------------------------------------
var g_screenLayout = null;
var g_scroll;
var g_priorvListTop;
var g_vListVisible = null;
var g_map;
var g_ieVersion;
var g_marker;

var userTypes = {
		Consumer	: {number:0, name:"Consumer", planType:"Advantage"},
		Repo		: {number:1, name:"Repo", planType:""},
		Dealer		: {number:2, name:"Dealer", planType:""}
}


var g_userType = userTypes.Dealer;
var dealerships = new Dealerships();
var vehicles = new Vehicles();

//INIT ----------------------------------------------------------
function testLogin()
{
	g_ieVersion = getInternetExplorerVersion();	

	setvListDimensions();
	postRequest(services.GetVehiclesByVINSuffix, '{"vinSuffix":"999999", "userType":' +g_userType.number+ '}',
		function(json, textStatus, jq) {
			init();
		});
}

function init() {
	setTimeout(function () {
		init2();
	}, 0);
}

function init2() {
	dealerships.requestList();
	
	// Workaround for lack of Android scrolling DIVs - employed for other browsers for consistency
	g_scroll = new iScroll("vlistContainer",{hscroll:false});
	
	if (~navigator.userAgent.indexOf("Firefox")) {
		window.history.replaceState('Object', 'Title', '.');
	}

	switch (g_userType) {
	case userTypes.Consumer:
		showOnly("vehicleList");
		vehicles.getList();
		break;
    case userTypes.Repo:
    	showOnly("vinEntry");
	break;
    case userTypes.Dealer:
    	showOnly("vinEntry");
    	show("locationSelect");
    	break;
    }
	

//	if (!~navigator.userAgent.indexOf("Android")) {
		$(window).resize(function() {
			if ((g_screenLayout != null) && (($(this).width() > minLandscapeWidth+100) && (g_screenLayout != ScreenLayouts.landscape)) ||
					(($(this).width() < minLandscapeWidth+100) && (g_screenLayout != ScreenLayouts.portrait))) {
				vehicles.showList();
			}
			setvListDimensions();
		});
//	}

}

function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

function showOnly(id) {
	hide("map_canvas");
	hide("vlistContainer");
	hide("help");
	hide("vinEntry");
	hide("locationSelect");
	hide("error");
	show(id);
}

function show(id) {
	if ((id=="btnHome") || (id=="btnHelp") || (id.substring(0,6)=="fence_" )) {
		document.getElementById(id).style.visibility = "visible";		
	} else {
		if (document.getElementById(id) !== null) { //added to suppress jscript errors, being called on non-existent id's
			document.getElementById(id).style.display = "block";
		}
	}

	if (id=="help") {
		hide("btnHelp");				// Help button should be hidden when help screen is displayed
		g_vlistVisible = isVisible("vlistContainer");
		hide("vlistContainer");
	}
	
    // If we're displaying the map, we need to reorganize the screen a little
	if (id=="map_canvas") {
		document.getElementById("banner").style.opacity = "0.7";

		show("btnHome");
		
		g_priorvListTop = document.getElementById("vlistContainer").style.top;
		document.getElementById("vlistContainer").style.position = "absolute";
		document.getElementById("vlistContainer").style.top = "auto";
		document.getElementById("vlistContainer").style.left = "4px";
		document.getElementById("vlistContainer").style.bottom = "4px";
		document.getElementById("vlistContainer").style.opacity = "0.7";

		// We're only going to show the selected vehicle, 
		// so hide all the separators.
		hideClass('separator');

		// Now hide all vehicles.
		hideClass('vehicle');		
	}

	// If showing the vehicle list, we MAY need to hide the map controls.
	showOrHide("map_controls");
	
	if (id == "vehicleList") {
		setvListDimensions();
	}

	
	if ((id == "vlistContainer") && (g_userType!=userTypes.Consumer)) {
		show("btnHome");
	}

	
	// If we are showing a vehicle and the map is displayed, add the refresh and close icons.
	if (~id.indexOf("vehicle_") && isVisible("map_canvas")) {
		var vehicleNumber = id.substr(8,id.length-7);
		show("refresh_"+vehicleNumber);
		show("close_"+vehicleNumber);
	}
}

function hide(id) {
	if ((id=="btnHome") || (id=="btnHelp") || (id.substring(0,6)=="fence_" )) {
		document.getElementById(id).style.visibility = "hidden";		
	} else {
		
		if (document.getElementById(id)!==null) { //put this check in, method was being called on non-existent elements
			document.getElementById(id).style.display = "none";
		}
	}

	if (id=="help") {
		show("btnHelp");				// Help button should be visible when help screen is not displayed.
	}
	

	if ((id == "vlistContainer") && (g_userType!=userTypes.Consumer)) {
		hide("btnHome");
	}

    // If we're hiding the map, we need to reorganize the screen a little
	if (id=="map_canvas") {

		document.getElementById("banner").style.opacity = "1";

		document.getElementById("vlistContainer").style.position = "absolute";
		if(g_priorvListTop!=null) {
			document.getElementById("vlistContainer").style.top = g_priorvListTop;
		}
//		document.getElementById("vlistCenter").style.left = "auto";
		document.getElementById("vlistContainer").style.opacity = "1";

		// We're only going to show the selected vehicle, 
		// so show all the separators.
		showClass('separator');

		// Now hide all close icons.
		hideClass("closeButton");

		// Now hide all refresh icons.
		hideClass("refreshContainer");

		// Now show all vehicles.
		showClass('vehicle');
		
		// Ensure vehicle list is displayed
		show("vehicleList"); 

	}

    showOrHide("map_controls");
}

function showOrHide(id) {
    /*
	if ((id=="map_controls") && (isVisible("map_canvas")) && (g_map!=null) ) {
		var visibility = ((isVisible("vehicleList")) && ($(window).height() < 400) ? false : true);

		var myOptions = {
				mapTypeControl:visibility,
				mapTypeControlOptions:{
					position:google.maps.ControlPosition.LEFT_CENTER
				},
				zoomControl:visibility,
				zoomControlOptions:{
					position:google.maps.ControlPosition.RIGHT_CENTER
				},
				streetViewControl:visibility,
				streetViewControlOptions:{
					position:google.maps.ControlPosition.LEFT_CENTER
				}
		}

		g_map.setOptions(myOptions);
	}
    */
}

function showClass(className) {
	var elements = new Array();
	elements = getElementsByClassName(className);
	for(i in elements ){
	     (className=="refreshContainer" ? elements[i].style.visibility = "visible" : elements[i].style.display = "block");
	}		
	setvListDimensions();
}

function hideClass(className) {
	var elements = new Array();
	elements = getElementsByClassName(className);
	for(i in elements ){
	     (className=="sdrefreshContainer" ? elements[i].style.visibility = "hidden" : elements[i].style.display = "none");
	}		
	setvListDimensions();
}

function isVisible(id) {
	return (((document.getElementById(id).style.display == "none") || (document.getElementById(id).style.visibility == "hidden")) ? false : true);
}

function setvListDimensions() {
	// Makes sure the list container is an appropriate height/width relative to the visible list.
	// Called whenever the list is modified, for example:
	//	- updating list of vehicles
	//  - hiding vehicles
	//  - showing vehicles
	//  - updating the address
	//  - resizing the screen

	$("#vlistContainer").css("maxHeight", ($(window).height()-$("#banner").height()-10)+"px");
	$("#vlistContainer").css("maxWidth", ($(window).width()-4)+"px");
	$("#vlistContainer").css("left", "0px");
	if (!isVisible("map_canvas")) {
		$("#vlistContainer").css("left", (($(window).width()/2)-($("#vlistContainer").width()/2))+"px");
	}
	
	if (g_scroll!=null) {
		setTimeout(function () {
			g_scroll.refresh();
		}, 0);
	}

	// Fix for 0 width (seen in IE8) 
	if ($("#vlistContainer").css("width")=="0px") {
		$("#vlistContainer").css("width",(($(window).width()/2)-($("#vlistContainer").width()/2))+"px");
	}

	// Fix for 0 height on map page(seen in IE8) 
	if (isVisible("map_canvas")) {
		// Force a height of 175px where size is zero
		if ($("#vlistContainer").css("height")=="0px") {
			$("#vlistContainer").css("height", "175px");
		}
	} else {
		// Revert back to default height
		$("#vlistContainer").css("height", "");
	}
	
	// IE8 workaround (i.e. use scrolling DIVs)
	if ((g_ieVersion>-1) && (g_ieVersion<9.0)) {
		$("#vlistContainer").css("overflow","auto");
	}
}

function getElementsByClassName(classname, node)  {
    if(!node) node = document.getElementsByTagName("body")[0];
    var a = [];
    var re = new RegExp('\\b' + classname + '\\b');
    var els = node.getElementsByTagName("*");
    for(var i=0,j=els.length; i<j; i++)
        if(re.test(els[i].className))a.push(els[i]);
    return a;
}

function formatDate(dateStr) {
	var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var d = new Date(parseInt(dateStr.substr(6)));
	d = new Date(d-(1000*60*d.getTimezoneOffset()));
	var hours = d.getHours()%12;
	var mins = d.getMinutes();
	return 	d.getDate() +" "+ month[d.getMonth()] +" @ "+ (hours==0 ? 12 : hours) +":"+(mins<10?"0":"")+mins+(d.getHours()<12?"am":"pm"); }

function help() {
	hide("vehicleList");
	hide("vinEntry");
	show("help");
	show("btnHome");
}

function back() {
	if (isVisible("help")) {
		hide("help");
	} else if (isVisible("map_canvas")) {
		hide("map_canvas");  // Hiding map canvas takes care of sorting out home and back icon visibility.

		switch (g_userType) {
 		case userTypes.Consumer:
			if ((g_vListVisible) || (!isVisible("map_canvas"))) {
				show("vlistContainer");
				show("vehicleList");
			}
			break;
		case userTypes.Dealer:
		case userTypes.Repo:
			if (vehicles.list.length < 2) {
				showOnly("vinEntry");
				if (g_userType==userTypes.Dealer) {
					show("locationSelect");
				}
				hide("btnHome")
			}
			break;
		}
	} else {
		switch (g_userType) {
		case userTypes.Dealer:
		case userTypes.Repo:
			showOnly("vinEntry");
			if (g_userType==userTypes.Dealer) {
				show("locationSelect");
			}
			hide("btnHome")
			break;
		}
	}

	if (!isVisible("map_canvas") && g_userType==userTypes.Consumer) {
		hide("btnHome");
	}
}

function showError(errorText) {
	document.getElementById("errorText").innerHTML = errorText;	
	showOnly("error");
}

//Web Services ----------------------------------------------------------
var services = {
	GetVehicles           	: {number:100, name:"GetVehicles"},
	GetLastLocation       	: {number:101, name:"GetLastLocation"},
	GetLastNLocations     	: {number:102, name:"GetLastNLocations"},
	RequestLocationUpdate 	: {number:103, name:"RequestLocationUpdate"},
	GetLocationsAfterId   	: {number:104, name:"GetLocationsAfterId"},
	GetLocationsAfterDate 	: {number:105, name:"GetLocationsAfterDate"},
	IsQuickFenceSet       	: {number:106, name:"IsQuickFenceSet"},
	SetQuickFence         	: {number:107, name:"SetQuickFence"},
	GetDashboard          	: {number:108, name:"GetDashboard"},
	GetDashboard          	: {number:109, name:"GetDashboard"},
	GetVehiclesByVINSuffix	: {number:200, name:"GetVehiclesByVINSuffix"},
	GetEvents               : {number:201, name:"GetEvents"},
	GetDealerships          : { number: 300, name: "GetAccessableDealerships" },
	GetFindITVehicles       : { name: "GetFindITVehicles"}
}

function postRequest(service, data, success)
{
	if (data==null) data='""';
	
	if (data!="") {
		vin=(eval('('+data+')').vin);
		vehicleNumber=vehicles.getID(vin);
	}

	var svcUrl = "Services/SkyLinkService.svc/json/";
	
	$.ajax(
			{
				type: "POST",
				url: svcUrl + service.name + '?q='+new Date().getTime(),
				dataType: "json",
				data: data,	
				contentType: "application/json; charset=utf-8",
				success: success,
				failure: function(json)
				{
				},
				error: function(json, textStatus, jq)
				{
					// If web service call returns an error, assume we need to login.
					if (~json.responseText.indexOf("Object reference not set to an instance of an object.")) {
						window.location.replace('main.aspx');
					} else {
						showError("Invalid response from web service.<br/>"+json+"<br/>"+textStatus+"<br/"+jq);
					}
				},
				complete: function(json)
				{
				}
			});
}


//VEHICLE ----------------------------------------------------------
function Vehicle(v) {
	this.vehicleNumber = v.vehicleNumber;
	this.vin = v.Vin;
	this.year = v.Year;
	this.make = v.Make;
	this.model = v.Model;
	this.name = (v.Name!=null ? v.Name : this.year+" "+this.make+" "+this.model);
	this.lastIgnStatus = null;
	this.lastTime = "";
	this.lastAddress = "";
	this.quickFenceStatus = null;
	
	this.locationNumber = 0;
	this.locTimer;
	this.fenceTimer;
	this.autoGetFenceStatus=false;

	this.html = html;
	this.htmlVehicleTable = htmlVehicleTable;
	this.htmlVehicleIcon = htmlVehicleIcon;
	this.htmlLocationIcon = htmlLocationIcon;
	this.htmlAddressDiv = htmlAddressDiv;
	this.htmlAddress = htmlAddress;
	this.setIcon = setIcon;
	this.showAddress = showAddress;
	this.setIgnStatus = setIgnStatus;
	this.showIgnStatus = showIgnStatus;
	this.updateLocation = updateLocation;
	this.toggleQuickFence = toggleQuickFence;
	this.setQuickFence = setQuickFence;
	this.checkForQuickFenceUpdate = checkForQuickFenceUpdate;
	this.isQuickFenceSet = isQuickFenceSet;
	this.endQuickFenceUpdate = endQuickFenceUpdate;
	this.getDashboard = getDashboard;
	
	this.plans = new Plans(this, v.SkyLinkPlan);

	this.locations = new Locations(this);	
	if (this.plans.planIsCurrent) {
		// Initialize vehicle locations with default "LOADING..."
		this.locations.add(0,{"StreetAddress":"LOADING...","LocationAlarmType":0});
	}

	this.color = v.Color.toLowerCase();
	if (this.color == "silver") {
		this.color = "gray";
	}

	if ((this.color != "red") 
			&& (this.color != "white") 
			&& (this.color != "blue") 
			&& (this.color != "green") 
			&& (this.color != "purple") 
			&& (this.color != "brown") 
			&& (this.color != "black") 
			&& (this.color != "yellow") 
			&& (this.color != "gray")) {
		this.color = "blue";
	}

	this.icon = "car_" + this.color;
	
	function html() {
			return "\
					<div id='vehicle_"+this.vehicleNumber+"' class='vehicle' style='font-size:large;'>" + (this.vehicleNumber>0 ? "<div class='separator'><hr/></div>" : "") +
			       "<span style='font-weight: bold;'>" + this.name + "</span>" +
				(g_userType != userTypes.Consumer ? "<br/><div id='vin'>"+this.vin+'</div>': "") +
				this.htmlVehicleTable() +"\
				</div>";
	}

	function htmlVehicleTable() {
		if (window.document.body.clientWidth > minLandscapeWidth) {
			g_screenLayout = ScreenLayouts.landscape;
			return "\
					 <table border=0 width=100%>\
					      <tr><td width=1px>" + this.htmlVehicleIcon() +"</td>\
					      <td>" + this.htmlAddressDiv() + "</td>\
					      <td width=1px>"+ this.htmlLocationIcon() +"</td>\
					    </tr>\
				      </table>\
				      ";
		} else {
			g_screenLayout = ScreenLayouts.portrait;
			return "\
					 <table border=0 width=100%>\
					    <tr>\
					      <td>\
					        <table>\
					          <tr>\
					            <td width=1px>" + this.htmlVehicleIcon() + "</td>\
							  </tr>\
							  <tr>\
							    <td width=1px>"+ this.htmlLocationIcon() +"</td>\
							  </tr>\
						    </table>\
						  </td><td>" + this.htmlAddressDiv() + "</td>\
					  </tr>\
					</table>\
					";
		}
	}
	
	//pulled this bit out of the outter div to disable setting geofence <div style='position:relative'"+ (this.plans.planIsCurrent ? "onclick='vehicles.list["+this.vehicleNumber+"].toggleQuickFence()'" : "") + ">
	function htmlVehicleIcon() {
		return "\
				<div style='position:relative'>\
		            <div class='fenceContainer'>\
				      <img class='vehicleIcon' src='images/" +this.icon + ".png' id='vehicle_" +this.vehicleNumber+ "'/>\
				      <img class='fenceIcon' src='images/quickfence.png' id='fence_" +this.vehicleNumber+ "'/>\
			        </div>\
			        " + (g_userType == userTypes.Consumer ? "<img  style='position:absolute; top:6px; left:23px; z-index:2; display:none;' id='fenceWait_"+this.vehicleNumber+"' src='images/wait30trans.gif' width='36px' height='36px'/>" : "") + "\
			      </div>\
			    "; 
	}
	
	function htmlLocationIcon() {
//        <img src='images/icon_locations.png' onclick='vehicles.showList()' style='width:64px; height:64px; padding:0px 10px 0px 10px'/>\

		
		return "\
		  <div style='position:relative'>\
				  "+ (this.plans.planIsCurrent ? "<img id=vehicleImage"+this.vehicleNumber+" src='images/icon_locations.png' onclick='vehicles.list[" +this.vehicleNumber+ "].locations.list[0].display("+this.vehicleNumber+")' style='width:64px; height:64px; padding:0px 10px 0px 10px'/>" : "") + "\
	        <div class='refreshContainer' id='refresh_"+this.vehicleNumber+"' >\
	          <img  class='refreshIcon' src='images/icon_refresh.png' width='48px' height='48px'/>\
	        </div>\
	        <img  style='position:absolute; top:-6px; left:24px; z-index:2; display:none' id='locWait_"+this.vehicleNumber+"' src='images/wait30trans.gif' width='36px' height='36px'/>\
	      </div>\
		    "; 
	}
	
	function htmlAddressDiv() {
		if (this.plans.planIsCurrent) {
			return "\
					<div class='address'>\
					  <div id='address_"+this.vehicleNumber+"'>" +this.htmlAddress()+ "</div>\
					  <div id='pageIndicator'>\
					    <img src='images/page_Previous.png' id='addPrev_"+this.vehicleNumber+"' onclick='vehicles.list["+this.vehicleNumber+"].showAddress(-1)' class='pageNavIcon'/>\
					    <div class='navContainer'>\
					      <div class='navPageActive' id='page0_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPageEmpty'  id='page1_"+this.vehicleNumber+"'>&nbsp;</div>\
					      <div class='navPageEmpty'  id='page2_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page3_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page4_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page5_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page6_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page7_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page8_"+this.vehicleNumber+"'>&nbsp;</div>\
						  <div class='navPageEmpty'  id='page9_"+this.vehicleNumber+"' style='margin-right:0px;'>&nbsp;</div>\
					    </div>\
					    <img src='images/page_Next.png' id='addNext_"+this.vehicleNumber+"' onclick='vehicles.list["+this.vehicleNumber+"].showAddress(1)' class='pageNavIcon'/>\
					  </div>\
					</div>\
				";
		} else {
			return "This vehicle does not have a current SkyLink PROTECT plan.<br/><a href='http://www.inilex.com/SkyLink/SkyLink.html' target='new'>Click here</a> to find out more";
		}
	}
	
	function htmlAddress() {
		if (this.locations.list.length > 0) {
			var l = this.locations.list[this.locationNumber];
			return "<table width=100%><tr><td width=1px>\
					<img src='images/icon_"+alertTypes[l.alertType].icon+".png' width='32px' height='32px' onclick=''/>\
					</td><td>"+l.time+"\
					<br/>"+l.address+"\
					</td></tr></table>";
		}
	}

	function setIcon() {
		document.getElementById("vehicle_" +this.vehicleNumber).src = "images/" +this.icon + ".png";
		(this.quickFenceStatus==true ? show("fence_" +this.vehicleNumber) : hide("fence_" +this.vehicleNumber));
	}

	function showAddress(locDelta) {
		if (this.locations.list.length > 0) {
			var llist = this.locations.list;
			var len = llist.length;

			if ((this.locationNumber + locDelta >= 0) && (this.locationNumber + locDelta < len)) {
				// Turn off current page indicator.
				document.getElementById("page" + this.locationNumber + "_" + this.vehicleNumber).className = "navPageFull";

				this.locationNumber += locDelta;
				l = llist[this.locationNumber];
				document.getElementById("address_" + this.vehicleNumber).innerHTML = this.htmlAddress();

				// Turn on current page indicator.
				document.getElementById("page" + this.locationNumber + "_" + this.vehicleNumber).className = "navPageActive";

				// Only plot the address if the vehicle is currently being displayed.
				if (isVisible("vehicle_" + this.vehicleNumber)) {
					l.plot();
				}

				setvListDimensions();
			}
		} else {
			$("#vehicle_" + this.vehicleNumber).find(".address").html("<div style='line-height:6.5;text-align:center'>no locations reported</div>");
			$("#vehicle_" + this.vehicleNumber).find("#vehicleImage" + this.vehicleNumber).hide();
			//document.getElementById("pageaddress_" + this.vehicleNumber).innerHTML = "<div style='text-align:center'>no locations on record</div>";
			//document.getElementById("page" + this.locationNumber + "_" + this.vehicleNumber).className = "navPageFull";
			

		}
	}
		
	function updateLocation() {
		// Don't allow another update request while one is in progress
		document.getElementById("refresh_"+this.vehicleNumber).onclick=function() { };
	
		show("locWait_"+this.vehicleNumber);
	
		this.locations.updateLocation();
	}
	
	
	function setIgnStatus (ignStatus) {
//		this.lastIgnStatus = (ignStatus ? "ON" : "OFF");
//		this.showIgnStatus(); 
	}

	function showIgnStatus(vehicleNumber) {
		document.getElementById("ignStatus_"+this.vehicleNumber).innerHTML = this.lastIgnStatus +")";
	}


function toggleQuickFence() {
		// Only perform toggle if we are in the mode of waiting for an auto update
		// (i.e. we are not waiting for a repsonse to a prior toggle request).
		if (this.autoGetFenceStatus) {
			this.quickFenceStatus = !this.quickFenceStatus;
			this.autoGetFenceStatus=false;						// Indicate that we are NOT doing an auto update of fence status.
			this.setQuickFence();
		}
	}

	function setQuickFence() {
		var _this = this;
		show("fenceWait_"+this.vehicleNumber);

		postRequest(services.SetQuickFence, '{"vin":"'+this.vin+'", "enable":'+this.quickFenceStatus+'}',
				function(json, textStatus, jq)
				{
			// Successfully requested to set quickfence

			// Since SetQuickFence WS does not return status of fence,
			// we need to request it to confirm it was properly set.
			clearTimeout(_this.fenceTimer);
			_this.fenceTimer = setTimeout(function(){_this.checkForQuickFenceUpdate(fenceStatusUpdateIterations)},fenceStatusUpdatePeriod);
				});
	}

	function checkForQuickFenceUpdate(iterations) {
		if (iterations < 1)
		{
			alert("Could not set QuickFence - try again later"); 
			hide("fenceWait_"+this.vehicleNumber);
		} else {
			this.isQuickFenceSet();
			var _this = this;
			this.autoGetFenceStatus=false;	// Indicate that we are NOT doing an auto update of fence status.
			clearTimeout(this.fenceTimer);
			this.fenceTimer = setTimeout(function(){_this.checkForQuickFenceUpdate(iterations-1)},fenceStatusUpdatePeriod);
		}
	}
	
	function isQuickFenceSet() {
		var _this = this;
		if (!_this.autoGetFenceStatus) {
			show("fenceWait_"+this.vehicleNumber);
		}
		
		postRequest(services.IsQuickFenceSet, '{"vin":"'+this.vin+'"}',
				function(json, textStatus, jq)
				{
			// If this is an auto update of fence status, then accept it for what it is
			// (otherwise, we initiated a toggle and therefore need to wait for it to change).
			if ((_this.autoGetFenceStatus) || (_this.quickFenceStatus==null))
			{
				_this.quickFenceStatus = eval('('+jq.responseText+')').d;
			}
			
			// Received quickfence status for a vehicle, so repopulate the icon.
			if (_this.quickFenceStatus == eval('('+jq.responseText+')').d)
			{
				_this.endQuickFenceUpdate();
			}
				}); 
	}

	function endQuickFenceUpdate() {
		hide("fenceWait_"+ this.vehicleNumber);
		this.setIcon();
		this.autoGetFenceStatus=true; // Indicate that we ARE doing an auto update of fence status.
		var _this = this;
		clearTimeout(this.fenceTimer);
		this.fenceTimer = setTimeout(function(){_this.isQuickFenceSet()},autoFenceCheckPeriod);
	}

	function getDashboard() {
		postRequest(services.GetDashboard, '{"vin":"'+this.vin+'"}',
				function(json, textStatus, jq)
				{});
	}
}


//VEHICLES ----------------------------------------------------------
function Vehicles() {

	//Public Functions
	this.init = init;
	this.getByVIN = getByVIN;
	this.getByVINSuffix = getByVINSuffix;
	this.getByLowBattery = getByLowBattery;
	this.getEvents = getEvents;
	this.getList = getList;
	this.addList = addList;
	this.showList = showList;
	this.getID = getID;

	this.init();

	function init() {
		this.list = [];	
	}

	function getByVIN() {
		document.getElementById("vehicleList").innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:20px; margin-bottom:20px'/>";
		showOnly("vlistContainer");

		var _this = this;
		postRequest(services.GetVehiclesByVIN, null,
				function(json, textStatus, jq) {
			_this.init();
			_this.addList(eval('('+jq.responseText+')').d);

			_this.showList();

			// We now have the list of vehicles, so for each vehicle...
			for (var i=0; i<_this.list.length; i++) {
				// Only interested in vehicles that have a current Protect plan.
				if (_this.list[i].plans.planIsCurrent) {
					_this.list[i].locations.getLastNLocations(); // ...get the list of last N locations ...
					_this.list[i].isQuickFenceSet();   // ... and the quickfence status for each vehicle.
				}
			}
		});
	}

	function getByLowBattery(Location) {
		document.getElementById("vehicleList").innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:20px; margin-bottom:20px'/>";
		showOnly("vlistContainer");
		
		var startDate = new Date();
		startDate.setDate(startDate.getDate()-1);  

		var startDateStr = "\\/Date("+startDate.getTime()+"-0700)\\/";

		this.getEvents('{"vFilter":{"area":{"id":0, "Radius":5, '+Location+'},"maxCount":30},"eFilter":{"locationAlertTypes":["Battery"],"maxCount":1,"startDate":"'+startDateStr+'"},"lastLocation":true,"userType":' +g_userType.number+ '}');
	}

	function getEvents(data) {

				var _this = this;
				postRequest(services.GetEvents, data,
						function(json, textStatus, jq) {
					_this.init();
					if (json.d.vehicles.length>0) {		// If vehicles were returned, show them.
						_this.addList(eval('('+jq.responseText+')').d.vehicles);
						_this.showList();

						// We now have the list of vehicles, so for each vehicle...
						for (var i=0; i<_this.list.length; i++) {
							// Only interested in vehicles that have a current Protect plan.
							if (_this.list[i].plans.planIsCurrent) {
								// Received locations for a vehicle, so repopulate the list.
								_this.list[i].locations.init();
								// Find eventList for this vehicle
								for (var e in json.d.eventList) {
									if (json.d.eventList[e].Vin==_this.list[i].vin) {
										// And show the most recent event for it.
										_this.list[i].locations.addList(json.d.eventList[e].Events);
										_this.list[i].locations.htmlPages();
										_this.list[i].showAddress(0);
									}
								}
							}
						}
					} else {
						showError("No matching vehicles found.");
					}
				});
	}

	function getByVINSuffix(searchTerm) {
		if (searchTerm.trim().length >= 1 ) {
			document.getElementById("vehicleList").innerHTML = "<img src='images/loading.gif' id='loading' style='margin-top:20px; margin-bottom:20px'/>";
			showOnly("vlistContainer");


			// TODO: Potentially awkward assumption here...
			// If we already have a vehicle that matches the suffix, then no need
			// to re-query the DB.  The awkwardness arrives if, say, we had a list of vehicles returned
			// but the one we really wanted wasn't included because, for example, it hadn't been put into
			// the right state.  The vehicle state is modified and we want to re-query the database to get
			// a new list, but it won't make the call because we already have the list.
			// This is an unlikely scenario.  To work around it, just refresh the page, or query 
			// another 6 digits string and then query this one again.
			//
			// this is where we need to pick up tomorrow
			// -- determine how many time being called for a search
			// -- we believe the intent of this is a caching of locations
			if ((this.list.length == 0) || (this.list[0].vin.substr(-searchTerm.length) != searchTerm)) {
				var _this = this;
				postRequest(services.GetFindITVehicles, '{"searchTerm":"' + searchTerm + '"}',
						function(json, textStatus, jq) {
					_this.init(); //tee hee, this sets the g_userType based on the current uri, ie. FindIt = DealerUser
					if (json.d.length>0) {		// If vehicles were returned, show them.
						_this.addList(eval('('+jq.responseText+')').d);
						_this.showList();

						// We now have the list of vehicles, so for each vehicle...
						for (var i=0; i<_this.list.length; i++) {
							// Only interested in vehicles that have a current Protect plan.
							if (_this.list[i].plans.planIsCurrent) {
								_this.list[i].locations.getLastNLocations(); // ...get the list of last N locations ...
								_this.list[i].isQuickFenceSet();   // ... and the quickfence status for each vehicle.
							}
						}
					} else {
						showError("No matching vehicles found.");
					}
				});
			} else { 
				this.showList();
			}
		}else {
			showError("Please enter a search term.");
		}
	}
	
	function getList() {
		var _this = this;
		postRequest(services.GetVehicles, null,
				function(json, textStatus, jq) {
			_this.init();
			_this.addList(eval('('+jq.responseText+')').d);

			_this.showList();

			// We now have the list of vehicles, so for each vehicle...
			for (var i=0; i<_this.list.length; i++) {
				// Only interested in vehicles that have a current Protect plan.
				if (_this.list[i].plans.planIsCurrent) {
					_this.list[i].locations.getLastNLocations(); // ...get the list of last N locations ...
					_this.list[i].isQuickFenceSet();   // ... and the quickfence status for each vehicle.
				}
			}
		});

	}

	function addList(vlist) {
		var color = "";
		var v;

		for (var i in vlist) {
			v = vlist[i];
			v.vehicleNumber = this.list.length; 
			this.list[v.vehicleNumber] = new Vehicle(v);
			//this.add(v.Vin, "car_"+color, v.Name, v.Year, v.Make, v.Model, null, "", "",null, v.currentPlan);
		}
	}

	function deprecated_add(v) {
		var vehicleNumber = this.list.length; 
		this.list[vehicleNumber] = new Vehicle(v);
//		this.list[vehicleNumber] = new Vehicle(vehicleNumber, vin,icon,name,year,make,model,lastIgnStatus,lastTime,lastAddress,quickFenceStatus,null,[], currentPlan);
	}

	function showList() {
		// If help screen or map are open , leave them up
		if ((!isVisible("help")) && (!isVisible("map_canvas"))) {
			showOnly("vlistContainer");
		}

		// Construct the vehicle list HTML.  Since we're referencing the
		// vehicleList DOM element to determine vehicle visibility, we
		// will wait to assign a new value to it until we've finished our loop.
		var wasHidden=[];
		var html = "";
		for (var v in this.list) {

			// If the vehicle is currently populated and hidden, 
			// make sure to keep it hidden once it is constructed
			var vElement = document.getElementById("vehicle_"+v);
			if (vElement != null) {
				if( !isVisible("vehicle_"+v)) {
					wasHidden[v]=true;
				}
			}

			html = html + this.list[v].html(0);
			//this.list.showIgnitionStatus();
		}

		// Now assign it to the DOM element.
		document.getElementById("vehicleList").innerHTML = html;

		// If the vehicle is currently populated and hidden, we need to make
		// sure to keep it hidden once it is constructed, so make a list.
		for (var v in this.list) {
			if(wasHidden[v]) {
				hide("vehicle_"+v); 
			} else {
				show("vehicle_"+v); 				
			}
			// Also, while we're here, ensure the fence icon is set appropriately
			this.list[v].setIcon();
			// And the page display...
			this.list[v].locations.htmlPages();
			// And the current page...
			this.list[v].showAddress(0);
		}

		try{
			// If we only have one vehicle in our list, then go straight to the map
			if (this.list.length === 1 &&
				this.list[0].locations.list.length !== 0 &&
				this.list[0].locations.list[0].address !== "LOADING...") //U.G.L.Y, I don't have no alibi other then it would take too long to shine this turd
			{
				this.list[0].locations.list[0].display(0);
			}
		} catch (e) {
			//forget abadit...
		}
		
		setvListDimensions();
	}

	function getID(vin) {
		for (var i=0; i<this.list.length; i++)
		{
			if (this.list[i].vin == vin) break
		}

		return (i<this.list.length ? i : -1);
	}
}


//LOCATION ----------------------------------------------------------
function Location(parent,id, l) {
	//Public Functions
	this.display = display;
	this.plot = plot;

	//Properties
	this.parent = parent;
	this.id = l.Id;

	var sep1 = "";
	var sep2 = "";
	var sep3 = "";

	if (l.StreetAddress!=null) {
		sep1 = ",";
	} else {
		l.StreetAddress = "";
	}

	if (l.City!=null) {
		sep2 = ",";
	} else {
		l.City = "";
	}

	if (l.State!=null) {
		sep3 = " ";
	} else {
		l.State = "";
	}

	l.Zip = (l.Zip!=null ? l.Zip: "");
	this.address = l.StreetAddress+(l.City!="" ? sep1 : "")+l.City+(l.State!="" ? sep2 : "")+l.State+(l.Zip!="" ? sep3 : "")+l.Zip;
	
	this.time = (l.TimeStamp!=null ? formatDate(l.TimeStamp) : "");
	this.latitude =  (l.Lat!=null ? l.Lat : 0.0);
	this.longitude = (l.Lon!=null ? l.Lon : 0.0);;
    this.alertType = l.LocationAlarmType;
    this.alertIndex = l.LocationAlarmIndex;
    this.speed = l.Speed;

	function display(vehicleNumber) {
		if (!isVisible("map_canvas")) {
			show("map_canvas");
			// We hid all vehicles when showing the map canvas,
			// so show the vehicle we're interested in.
			show("vehicle_"+vehicleNumber);
			
			// Setup the location button to now refresh (update) the location.
			document.getElementById("refresh_"+vehicleNumber).onclick=function() { vehicles.list[vehicleNumber].updateLocation(); };
			this.plot();
		}
	}

	function plot() {
		// Only show location if the map is actually visible AND it is not a dummy location used during loading
		if (isVisible("map_canvas") && (this.address!='LOADING...')) {  
			var myLatlng = [this.latitude, this.longitude];
			var zoomFactor = 12 			// Default zoom factor

			var layers;

			if (MAPS_PROVIDER == "bing")
			{
			    layers = {
			        "Road" : L.bingLayer(MAP_API_KEY, { type: 'Road' }),
			        "Aerial": L.bingLayer(MAP_API_KEY, { type: 'AerialWithLabels' })
			    }
			}
			else if (MAPS_PROVIDER == "mapbox")
			{
			    layers = {
			        "Road": L.tileLayer(MAPBOX_BASEMAPSTREET + '?access_token=' + MAP_API_KEY, {
						attribution: decodeURIComponent(MAP_ATTRIBUTION),
						tileSize: 512,						
						zoomOffset: -1
			        }),
			        "Aerial": L.tileLayer(MAPBOX_BASEMAPSATELLITE + '?access_token=' + MAP_API_KEY, {
						attribution: decodeURIComponent(MAP_ATTRIBUTION),
						tileSize: 512,
						zoomOffset: -1
			        })
			    }
			}

			if (!g_map) {
			    var myOptions = {
			        zoom: zoomFactor,
			        center: myLatlng,
			        layers: [layers.Road, layers.Aerial],
			    };
			    g_map = L.map(document.getElementById("map_canvas"), myOptions);
			    L.control.layers(layers).addTo(g_map);
			    showOrHide("map_controls");
			}
			
			var maxZoom = g_map.getMaxZoom();

			if (g_marker) {
			    g_map.removeLayer(g_marker);
			}
            
			if (MAPS_PROVIDER == "bing") {
			    var images = Z.imagery(MAP_API_KEY),
                    northPole = Z.geopoint(90, 0),
                    northPoleImages = images(northPole),
                    vehicleLocation = Z.geopoint.apply(null, myLatlng),
                    vehicleLocationImages = images(vehicleLocation),
                    maxZoom = Z.maxZoom(northPoleImages);
			}
			
			g_marker = L.marker(myLatlng, {
				title:this.address
			});

			g_marker.on('click', function() {
			    show("vlistContainer");
			});

			if (MAPS_PROVIDER == "mapbox")
			    g_map.setView(myLatlng, maxZoom);
			else
			    g_map.setView(myLatlng);

			g_marker.addTo(g_map);
		}
	}
}

function Dealerships(parent)
{
	this.parent = parent;
	
	//public functions
	this.init = init;
	this.requestList = requestList;
	this.requestBatteryAlerts = requestBatteryAlerts;
	this.add = add;
	
	function init(){
		this.list = [];
	}
	
	function requestList(){
		this.init();
		$("#dealershipSingle").hide();
		$("#dealershipSelect").hide();
		$("#dealershipWorking").show();
		var _this = this;
		postRequest(services.GetDealerships, null,
				function(json, textStatus, jq)
				{
					$("#dealershipWorking").hide();
					for (var dealership in json.d)
					{
						_this.add(json.d[dealership]);
					}
					
					if (_this.list.length == 1)
					{
						$("#dealershipSingle").show();
					}
					else if (_this.list.length > 1)
					{
						var html = "";
						for (var i in _this.list)
						{
							html += '<option value="' + i + '">' + _this.list[i].Key + '</option>';
						}
						$("#location").html(html);
						$("#dealershipSelect").show();
					}
				});

	}
	
	function add(dealer){
		this.list.push(dealer);
	}
	
	function requestBatteryAlerts(index){
		vehicles.getByLowBattery('"Lat":' + this.list[index].Value.Lat + ',"Lon":' + this.list[index].Value.Lon);
	}
}

//LOCATIONS ----------------------------------------------------------
/*
var alertTypes = new Array(
		  {number:0, name:"Location", icon:"locations"},
		  {number:1, name:"Ignition On", icon:"alertIgnOn"},
		  {number:2, name:"Ignition Off", icon:"alertIgnOff"},
		  {number:3, name:"Entered Geofence", icon:"alertGeofence"},
		  {number:4, name:"Left Geofence", icon:"alertGeofence"},
		  {number:5, name:"Speed Threshold Exceeded", icon:"alertSpeed"},
	      {number:6, name:"Low Battery Level", icon:"alertBattery"},
          {number:7, name:"Early Theft Detection", icon:"alertQuickfence"},
          {number:8, name:"Power Disconnected", icon:"alertPowerOff"}
        );
*/

var alertTypes = new Array(
		  {number:0, name:"Location", icon:"locations"},
		  {number:1, name:"Ignition On", icon:"alertIgnOn"},
		  {number:2, name:"Ignition Off", icon:"alertIgnOff"},
		  {number:3, name:"Entered Geofence", icon:"alertGeofence"},
		  {number:4, name:"Left Geofence", icon:"alertGeofence"},
		  {number:5, name:"Low Battery Level", icon:"alertBattery"},
		  {number:6, name:"Early Theft Detection", icon:"alertQuickfence"},
		  {number:7, name:"Speed Threshold Exceeded", icon:"alertSpeed"},
		  {number:8, name:"Power Disconnected", icon:"alertPowerOff"}
        );

function Locations(parent) {
    this.parent = parent;

	// Public Functions
	this.init = init;
	this.add = add;
	this.addList = addList;
	this.htmlPages = htmlPages;
	this.insert = insert;
	this.shift = shift;
	this.updateLocation = updateLocation;
	this.getLastLocation = getLastLocation;
	this.getLastNLocations = getLastNLocations;
	this.requestLocationUpdate = requestLocationUpdate;
	this.getLocationsAfterId = getLocationsAfterId;
	this.checkForLocationUpdate = checkForLocationUpdate;
	this.endLocationUpdate = endLocationUpdate;

	this.locationUpdateRequested=false;

	this.init();

	
	function init() {
		this.list = [];	
	}

	function addList(llist) {
		for (var i in llist) {
			l = llist[i];
			this.add(i, l);
		}
		/*
		// Used for creating marketing materials
		if (this.parent.vehicleNumber==0) {
			this.list[0].address = '460 S Benson Lane, Chandler, AZ 85224'; 
			this.list[0].latitude = '33.295319'; 
			this.list[0].longitude = '-111.886761';
		} else {
			this.list[0].address = '401 East Jefferson Street, Phoenix, AZ 85004'; 
			this.list[0].latitude = '37.0625'; 
			this.list[0].longitude = '-95.677068';
		}
		*/
		//pp 2/12/13
		if (this.list.length > 0) {
			this.parent.setIgnStatus(this.list[0].IgnitionOn);
		}
	}

	function add(locationNumber, l) {
		this.list[locationNumber] = new Location(this, locationNumber, l);
	}

	function insert(llist) {
		this.shift(llist.length);
		this.addList(llist);
	}

	function shift(numItems) {
		n = this.list.length;
		for (var i=(n>9 ? 8 : n-1); i>=numItems-1; i--)
		{
			this.list[i+1] = this.list[i];
		}
	}

	function html(locationNumber) {
		l = this.list[locationNumber];
		return "\
				<div id='location'>\
				<img class='warningIcon' src='images/icon_locations.png' width='64px' height='64px' />\
				<span style='font-weight: bold; font-size: 22px;'>" + l.address + "</span>\
				<br/>" + l.time + "<br/>\
				<p class='clearFloat' />\
				<hr />\
				</div>";
	}

	function htmlPages() {
		if (this.list.length > 0) {
			// Set "loaded" and "unloaded" page indicators
			var len=this.list.length;
			for (var i=0; i<10; i++) {
				document.getElementById("page"+i+"_"+this.parent.vehicleNumber).className = "navPage"+ (i<len ? "Full" : "Empty");
			}
		}
	}
	
	function updateLocation(vehicle) {
	/* 1. We must first get most current locations (so we don't confuse
	      locations that have arrived at the server since the last update 
	      on the client with the update just requested.
	   2. Then issue a location update request.
	   3. When location update request returns, we need to periodically
	      check for new locations.  Anything reported will be the new
	      location.
	*/
		var _this = this;

		this.locationUpdateRequested = true;
		
		postRequest(services.GetLocationsAfterId, '{"vin":"'+this.parent.vin+'","locationId":' + this.list[0].id + '}',
				function(json, textStatus, jq)
				{

			// Received list of more recent locations than we know of.
			
			// If this list contains any new locations (i.e. is not empty) update 
			// the locations for this vehicle and request the update.
			if (json.d.length>0) {
				_this.insert(json.d);
			} 
			_this.requestLocationUpdate();
				});
	}
	
	function requestLocationUpdate()
	{
		var _this = this;
		postRequest(services.RequestLocationUpdate, '{"vin":"'+this.parent.vin+'"}',
				function(json, textStatus, jq)
				{
			_this.getLocationsAfterId();
				});
	}
	
	function getLocationsAfterId() {
		var _this = this;
		
		postRequest(services.GetLocationsAfterId, '{"vin":"'+this.parent.vin+'","locationId":' + this.list[0].id + '}',
				function(json, textStatus, jq)
				{
			// Received list of more recent locations than we know of.
			
			// If this list contains any new locations (i.e. is not empty) update 
			// the locations for this vehicle and stop subsequent requests.
			if (json.d.length>0)
			{
				_this.insert(json.d);
				_this.htmlPages();
				_this.parent.showAddress(-_this.parent.locationNumber);
				_this.list[0].plot();
				_this.endLocationUpdate(_this);				
			} else {
				// Check periodically for new location to arrive
				if (_this.locationUpdateRequested == true) {	// i.e. this is the response to an update request from the user.
					clearTimeout(_this.locTimer);
					_this.locTimer = setTimeout(function(){_this.checkForLocationUpdate(locationUpdateIterations)},locationUpdatePeriod);
				} else {
					_this.endLocationUpdate(_this);				// For automated updates, we don't need to cycle.
				}
			}
				});
	}

	function checkForLocationUpdate(iterations) {
		if (iterations < 1)
		{
			//alert("Could not update location - try again later"); 
			endLocationUpdate(this);
		} else {
			iterations = iterations-1;
			this.getLocationsAfterId();
		}
		return;
	}

	function endLocationUpdate(_this) {
		_this.locationUpdateRequested = false;
		hide("locWait_"+_this.parent.vehicleNumber);

		// If we are on the map screen, make sure to reset the onclick to get location updates.
		if (isVisible("map_canvas")) {
			document.getElementById("refresh_"+_this.parent.vehicleNumber).onclick=function() { _this.parent.updateLocation(); };
		}

		clearTimeout(_this.locTimer);
		_this.locTimer = setTimeout(function(){_this.getLocationsAfterId()},autoLocationCheckPeriod);
	}

	function getLastLocation()
	{
		var _this = this;
		postRequest(services.GetLastLocation, '{"vin":"'+this.parent.vin+'"}',
				function(json, textStatus, jq)
				{});
	}

	function getLastNLocations() {
		var _this = this;
		show("locWait_"+this.parent.vehicleNumber);
		postRequest(services.GetLastNLocations,'{"vin":"' + this.parent.vin + '","numberOfLocations":10}',
				function(json, textStatus, jq)
				{
			// Received locations for a vehicle, so repopulate the list.
			_this.init();
			_this.addList(eval('('+jq.responseText+')').d);

			_this.htmlPages();
			_this.parent.showAddress(0);
	
			// And clean up...
			_this.endLocationUpdate(_this);

				});
	}

}

//PLANS ----------------------------------------------------------
function Plans(parent, plans) {
  this.parent = parent;
  this.planIsCurrent = isProtectPlanCurrent(plans);

	function isProtectPlanCurrent(plans) {
		var planValidity = false;
		
		switch (g_userType) {
		case userTypes.Consumer:
			// Does this vehicle have a current "Advantage" (i.e. protect) plan?
			var now = new Date().getTime();
			var expirationDate;
	
			for (var p in plans) {
				var d = new Date(parseInt(plans[p].ExpirationDate.substr(6)));
				d = new Date(d-(1000*60*d.getTimezoneOffset()));
				expirationDate = d.getTime();
	
				if ((~plans[p].Name.indexOf("Advantage")) && (now < expirationDate))
				{
					planValidity = true;
					break;
				}
			}
			break;
		case userTypes.Repo:
		case userTypes.Dealer:
		  planValidity = true;
		  break;
		}

		return planValidity;
	}
}
