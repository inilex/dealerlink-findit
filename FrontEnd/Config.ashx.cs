﻿namespace FrontEnd
{
    using System.Web;
    using FrontEnd.Properties;

    public sealed class Config : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/javascript";
            context.Response.Write(string.Format("var MAP_API_KEY = \"{0}\";", Settings.Default.MapApiKey));
            context.Response.Write(string.Format("var MAPS_PROVIDER = \"{0}\";", Settings.Default.MapsProvider));
            context.Response.Write(string.Format("var MAP_ATTRIBUTION = \"{0}\";", Settings.Default.GenericMapAttrib));
            if(Settings.Default.MapsProvider.Equals("mapbox"))
            {
                context.Response.Write(string.Format("var MAPBOX_BASEMAPSTREET = \"{0}\";", Settings.Default.MapBoxBaseMapStreet));
                context.Response.Write(string.Format("var MAPBOX_BASEMAPSATELLITE = \"{0}\";", Settings.Default.MapBoxBaseMapSatellite));
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}