﻿window.Z = (function ($, Q, R) {
    function geopoint(lat, lon) {
        return { lat: lat, lon: lon };
    }

    function imageData(url) {
        var deferred = Q.defer();
        $.ajax({
            type: "GET",
            url: url,
            success: deferred.resolve
        });
        return deferred.promise;
    }

    var imagery = R.curry(function (bingKey, geopoint, zoom) {
        var url = "https://dev.virtualearth.net/REST/v1/Imagery/Metadata/AerialWithLabels/" + geopoint.lat + "," + geopoint.lon + "?zl=" + zoom + "&key=" + bingKey + "&jsonp=?";
        var deferred = Q.defer();
        $.ajax({
            type: "GET",
            url: url,
            dataType: "jsonp",
            success: function (metadata) {
                deferred.resolve(metadata.resourceSets[0].resources[0].imageUrl);
            }
        });
        return deferred.promise;
    });

    function maxZoomFactory(northPoleImages) {
        return function maxZoom(subjectImages, minimum) {
            minimum = minimum || 0;
            var deferred = Q.defer();
            Q.all([
                northPoleImages(21).then(imageData),
                subjectImages(minimum + 1).then(imageData)
            ])
            .then(function (images) {
                if (images[0] === images[1]) {
                    deferred.resolve(minimum);
                }
                else {
                    maxZoom(subjectImages, minimum + 1)
                        .then(deferred.resolve);
                }
            });
            return deferred.promise;
        };
    }

    return {
        geopoint: geopoint,
        imagery: imagery,
        maxZoom: maxZoomFactory
    };
})($, Q, R);