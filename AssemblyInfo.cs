using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Inilex")]
[assembly: AssemblyProduct("FindIT")]
[assembly: AssemblyCopyright("Copyright � Inilex 2015")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("2.2.1")]