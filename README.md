Release History
===============

Version 2.2.0 06/21/2016
------------------------

* Added support for Mapbox mapping via leaflet

Version 2.1.0 06/03/2015
------------------------

* SMTP mail settings are being used in web.config vs. being hard coded

Version 2.0.0 06/03/2015
------------------------

* Converted mapping to use Leaflet + Bing.

Version 1.0.0 05/20/2015
------------------------

* Initialized codebase (hopefully) to match production
* Fixed recover password error
* Added the ability to search for vehicles by the Stock Number
* Removed six-character limit on search term